//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 3.1b - 2014   //
//--------------------------//
//    DAC_Config_Creator    //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

//////////////////////////////////////////////////////////
// Modified by Monsoon to be headless client compatible //
//////////////////////////////////////////////////////////

isHC = if(!isServer && !hasInterface) then{ True } else{ False };

_scr = [] spawn (compile preprocessFile "DAC\Scripts\DAC_Preprocess.sqf");
waituntil {scriptdone _scr};

scalar = "any";DAC_Init_Camps = 0;

waituntil{time > 0.3};

if(isServer || isHC) then {if(local player) then {DAC_Code = 1} else {DAC_Code = 0}} else {if(isnull player) then {DAC_Code = 3} else {DAC_Code = 2}};

//===============|
// DAC_Settings	 |
//===============|=============================================================================================|

	if(isNil "DAC_STRPlayers")		then {		DAC_STRPlayers		= [
	"UnitNATO_CO","UnitNATO_CO_M","UnitNATO_CO_UAV","UnitNATO_CO_D","UnitCSAT_CO","UnitCSAT_CO_M","UnitCSAT_CO_UAV","UnitAAF_CO","UnitAAF_CO_M","UnitAAF_CO_UAV","UnitAAF_CO_D",
	"UnitNATO_DC","UnitNATO_DC_RTO","UnitNATO_DC_D","UnitNATO_DC_M","UnitNATO_ASL_SL","UnitNATO_ASL_RTO","UnitNATO_ASL_AR","UnitNATO_ASL_AAR","UnitNATO_ASL_FTL","UnitNATO_ASL_AAR_1",
	"UnitNATO_ASL_AT","UnitNATO_ASL_AT_1","UnitNATO_ASL_R","UnitNATO_ASL_M","UnitNATO_BSL_SL","UnitNATO_BSL_RTO","UnitNATO_BSL_AAR","UnitNATO_BSL_AR","UnitNATO_BSL_AAR_1","UnitNATO_BSL_FTL",
	"UnitNATO_BSL_AT","UnitNATO_BSL_AT_1","UnitNATO_BSL_R","UnitNATO_BSL_M","UnitNATO_CSL_SL","UnitNATO_CSL_RTO","UnitNATO_CSL_AR","UnitNATO_CSL_AAR","UnitNATO_CSL_AAR_1","UnitNATO_CSL_FTL",
	"UnitNATO_CSL_AT","UnitNATO_CSL_AT_1","UnitNATO_CSL_R","UnitNATO_CSL_M","UnitNATO_TH1_P","UnitNATO_TH1_G1","UnitNATO_TH2_P","UnitNATO_TH2_G1","UnitNATO_TH3_P","UnitNATO_TH3_G1",
	"UnitNATO_TH4_P","UnitNATO_TH4_G1","UnitNATO_IFV1_C","UnitNATO_IFV1_D","UnitNATO_IFV1_G","UnitNATO_IFV2_C","UnitNATO_IFV2_D","UnitNATO_IFV2_G","UnitNATO_IFV3_C","UnitNATO_IFV3_D",
	"UnitNATO_IFV3_G","UnitNATO_IFV4_C","UnitNATO_IFV4_D","UnitNATO_IFV4_G","UnitNATO_AH1_P","UnitNATO_AH1_CP","UnitNATO_TNK1_C","UnitNATO_TNK1_G","UnitNATO_TNK1_D","UnitNATO_TNK1_L",
	"UnitNATO_TNK2_C","UnitNATO_TNK2_D","UnitNATO_TNK2_G","UnitNATO_TNK2_L","UnitNATO_TNK3_C","UnitNATO_TNK3_D","UnitNATO_TNK3_G","UnitNATO_TNK3_L","UnitNATO_ENG1_FTL","UnitNATO_ENG1_A2",
	"UnitNATO_ENG1_A1_1","UnitNATO_ENG1_A1_2","UnitNATO_ENG1_A1","UnitNATO_ENG1_A3","UnitNATO_MMG1_FTL","UnitNATO_MMG1_G","UnitNATO_MMG1_AG","UnitNATO_MAT1_FTL","UnitNATO_MAT1_G","UnitNATO_MAT1_AG",
	"UnitNATO_HMG1_FTL","UnitNATO_HMG1_G","UnitNATO_HMG1_G","UnitNATO_HAT1_FTL","UnitNATO_HAT1_G","UnitNATO_HAT1_AG","UnitNATO_MTR1_FTL","UnitNATO_MTR1_G","UnitNATO_MTR1_AG","UnitNATO_MSAM1_FTL",
	"UnitNATO_MSAM1_G","UnitNATO_MSAM1_AG","UnitAAF_DC","UnitAAF_DC_RTO","UnitAAF_DC_D","UnitAAF_DC_M","UnitAAF_ASL_SL","UnitAAF_ASL_RTO","UnitAAF_ASL_AR","UnitAAF_ASL_AAR","UnitAAF_ASL_R","UnitAAF_ASL_R_1",
	"UnitAAF_ASL_M","UnitAAF_ASL_R_2","UnitAAF_ASL_FTL","UnitAAF_ASL_AAR_1","UnitAAF_BSL_SL","UnitAAF_BSL_AR","UnitAAF_BSL_AAR_1","UnitAAF_BSL_R","UnitAAF_BSL_R_1","UnitAAF_BSL_M",
	"UnitAAF_BSL_RTO","UnitAAF_BSL_AAR","UnitAAF_BSL_FTL","UnitAAF_BSL_R_2","UnitAAF_CSL_SL","UnitAAF_CSL_AR","UnitAAF_CSL_AAR_1","UnitAAF_CSL_R_1","UnitAAF_SL_R_2","UnitAAF_CSL_M","UnitAAF_CSL_RTO","UnitAAF_CSL_AAR","UnitAAF_CSL_FTL",
	"UnitAAF_CSL_R_3","UnitAAF_MMG1_FTL","UnitAAF_MMG1_AG","UnitAAF_MMG1_G","UnitAAF_MAT1_FTL","UnitAAF_MAT1_G","UnitAAF_MAT1_AG","UnitAAF_HMG1_FTL","UnitAAF_HMG1_G","UnitAAF_HMG1_AG","UnitAAF_HAT1_FTL","UnitAAF_HAT1_G","UnitAAF_HAT1_AG",
	"UnitAAF_MTR1_FTL","UnitAAF_MTR1_G","UnitAAF_MTR1_AG","UnitAAF_MSAM1_FTL","UnitAAF_MSAM1_G","UnitAAF_MSAM1_AG","UnitAAF_AH1_P_1","UnitAAF_AH1_CP_1","UnitAAF_TH1_P_1","UnitAAF_TH1_CP_1","UnitAAF_TH2_P_1","UnitAAF_TH2_CP_1","UnitAAF_TH3_P_1",
	"UnitAAF_TH3_CP_1","UnitAAF_TH4_P_1","UnitAAF_TH4_CP_1","UnitAAF_TNK1_C_1","UnitAAF_TNK1_D_1","UnitAAF_TNK1_G_1","UnitAAF_TNK2_C_1","UnitAAF_TNK2_D_1","UnitAAF_TNK2_G_1","UnitAAF_TNK3_C_1","UnitAAF_TNK3_G_1","UnitAAF_TNK3_D_1","UnitAAF_IFV1_C_1",
	"UnitAAF_IFV1_D_1","UnitAAF_IFV1_G_1","UnitAAF_IFV2_C_1","UnitAAF_IFV2_G_1","UnitAAF_IFV2_D_1","UnitAAF_IFV3_C_1","UnitAAF_IFV3_G_1","UnitAAF_IFV3_D_1","UnitAAF_IFV4_C_1","UnitAAF_IFV4_G_1","UnitAAF_IFV4_D_1","UnitCSAT_DC","UnitCSAT_DC_RTO",
	"UnitCSAT_DC_D","UnitCSAT_DC_M","UnitCSAT_ASL_SL","UnitCSAT_ASL_RTO","UnitCSAT_ASL_AR","UnitCSAT_ASL_FTL","UnitCSAT_ASL_R_1","UnitCSAT_ASL_M","UnitCSAT_ASL_AAR","UnitCSAT_ASL_AAR_1","UnitCSAT_ASL_AT_1","UnitCSAT_ASL_AT_2","UnitCSAT_BSL_SL",
	"UnitCSAT_BSL_RTO","UnitCSAT_BSL_AR","UnitCSAT_BSL_FTL","UnitCSAT_BSL_R","UnitCSAT_BSL_M","UnitCSAT_BSL_AAR","UnitCSAT_BSL_AAR_1","UnitCSAT_BSL_AT_1","UnitCSAT_BSL_AT_2","UnitCSAT_CSL_SL","UnitCSAT_CSL_RTO","UnitCSAT_CSL_AR","UnitCSAT_CSL_FTL",
	"UnitCSAT_CSL_R1","UnitCSAT_CSL_M","UnitCSAT_CSL_AAR","UnitCSAT_CSL_AAR_1","UnitCSAT_CSL_AT_1","UnitCSAT_CSL_AT_2","UnitCSAT_MMG1_FTL","UnitCSAT_MMG1_G","UnitCSAT_MMG1_AG","UnitCSAT_MAT1_FTL","UnitCSAT_MAT1_G","UnitCSAT_MAT1_AG","UnitCSAT_HMG1_FTL",
	"UnitCSAT_HMG1_G","UnitCSAT_HMG1_AG","UnitCSAT_HAT1_FTL","UnitCSAT_HAT1_G","UnitCSAT_HAT1_AG","UnitCSAT_MTR1_FTL","UnitCSAT_MTR1_G","UnitCSAT_MSAM1_FTL","UnitCSAT_MSAM1_G","UnitCSAT_MSAM1_AG","UnitCSAT_AH1_P","UnitCSAT_AH1_CP","UnitCSAT_TH1_P",
	"UnitCSAT_TH1_CP","UnitCSAT_TH2_P","UnitCSAT_TH2_CP","UnitCSAT_TH3_P","UnitCSAT_TH3_CP","UnitCSAT_TH4_P","UnitCSAT_TH4_CP","UnitCSAT_IFV1_C","UnitCSAT_IFV1_G","UnitCSAT_IFV1_D","UnitCSAT_IFV2_C","UnitCSAT_IFV2_G","UnitCSAT_IFV2_D","UnitCSAT_IFV3_C",
	"UnitCSAT_IFV3_G","UnitCSAT_IFV3_D","UnitCSAT_IFV4_C","UnitCSAT_IFV4_G","UnitCSAT_IFV4_D","UnitCSAT_TNK1_C","UnitCSAT_TNK1_G","UnitCSAT_TNK1_D","UnitCSAT_TNK2_C","UnitCSAT_TNK2_G","UnitCSAT_TNK2_D","UnitCSAT_TNK3_C","UnitCSAT_TNK3_G","UnitCSAT_TNK3_D",
	"UnitAAF_ENG1_FTL","UnitAAF_ENG1_A2","UnitAAF_ENG1_A1_2","UnitAAF_ENG1_A1_3","UnitAAF_ENG1_A1","UnitAAF_ENG1_A3","UnitCSAT_ENG1_FTL_1","UnitCSAT_ENG1_A2_1","UnitCSAT_ENG1_A1_2","UnitCSAT_ENG1_A1_3","UnitCSAT_ENG1_A1_1","UnitCSAT_ENG1_A3_1",
	"UnitAAF_DSL_SL","UnitAAF_DSL_RTO","UnitAAF_DSL_AAR","UnitAAF_Dog_FTL","UnitAAF_DSL_R","UnitAAF_DSL_AR","UnitAAF_DSL_AAR_1","UnitAAF_DSL_R_2","UnitAAF_DSL_R_3","UnitAAF_DSL_M"]};
	
	
	if(isNil "DAC_AI_Count_Level")	then {		DAC_AI_Count_Level  = [[2,4],[3,6],[4,8],[8,14],[1,2]]			};
	if(isNil "DAC_Dyn_Weather") 	then {		DAC_Dyn_Weather		= [0,0,0,[0, 0, 0],0]						};
	if(isNil "DAC_Reduce_Value") 	then {		DAC_Reduce_Value	= [1000,1050,0.3]							};
	if(isNil "DAC_AI_Spawn") 		then {		DAC_AI_Spawn		= [[10,5,5],[10,5,15],1,0,250,1]			};
	if(isNil "DAC_Delete_Value") 	then {		DAC_Delete_Value	= [[300,100],[300,100],600]					};
	if(isNil "DAC_Del_PlayerBody") 	then {		DAC_Del_PlayerBody	= [300,100]									};
	if(isNil "DAC_Com_Values") 		then {		DAC_Com_Values		= [0,1,0,0]									};
	if(isNil "DAC_AI_AddOn") 		then {		DAC_AI_AddOn		= 1											};
	if(isNil "DAC_AI_Level") 		then {		DAC_AI_Level		= 3											};
	if(isNil "DAC_Res_Side") 		then {		DAC_Res_Side		= 0											};
	if(isNil "DAC_Marker") 			then {		DAC_Marker			= 0											};
	if(isNil "DAC_WP_Speed") 		then {		DAC_WP_Speed		= 0.01										};
	if(isNil "DAC_Join_Action")		then {		DAC_Join_Action		= false										};
	if(isNil "DAC_Fast_Init") 		then {		DAC_Fast_Init		= false										};
	if(isNil "DAC_Player_Marker")	then {		DAC_Player_Marker	= false										};
	if(isNil "DAC_Direct_Start")	then {		DAC_Direct_Start	= false										};
	if(isNil "DAC_Activate_Sound")	then {		DAC_Activate_Sound	= false										};
	if(isNil "DAC_Auto_UnitCount")	then {		DAC_Auto_UnitCount	= [15,10]									};
	if(isNil "DAC_Player_Support")	then {		DAC_Player_Support	= [10,[4,2000,3,1000]]						};
	if(isNil "DAC_SaveDistance")	then {		DAC_SaveDistance	= [250,["DAC_Save_Pos"]]					};
	if(isNil "DAC_Radio_Max")		then {		DAC_Radio_Max		= DAC_AI_Level								};
		
		
	DAC_BadBuildings 	= 	[];
	DAC_GunNotAllowed	= 	[];
	DAC_VehNotAllowed	= 	[];
	DAC_Locked_Veh		=	[];
	DAC_SP_Soldiers		=	["rhsusf_army_ocp_autorifleman","rhsusf_army_ocp_machinegunner","rhsusf_usmc_marpat_d_machinegunner","rhsusf_usmc_marpat_d_autorifleman_m249","PSZ_PL_WDL10_Soldier_MG","PSZ_PL_DES10_Soldier_MG","usm_ranger_90s_w_h_mg","usm_ranger_90s_w_h_ar","usm_soldier_90s_d_h_mg","usm_soldier_90s_d_h_ar","rhsgref_cdf_b_reg_machinegunner","rhsgref_hidf_autorifleman","rhsgref_hidf_machinegunner","rhs_msv_machinegunner","rhs_vdv_arifleman","rhs_vdv_des_arifleman","LOP_TKA_Infantry_MG","LOP_SLA_Infantry_MG","rhsgref_ins_g_machinegunner","rhsgref_nat_pmil_machinegunner","LOP_UA_Infantry_MG","rhsgref_cdf_un_machinegunner","LOP_UN_Infantry_MG","LOP_AFR_Infantry_AR","LOP_AM_Infantry_AR","LOP_RACS_Infantry_MG"];
	DAC_Data_Array 		= 	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,[]];
	DAC_Marker_Val		= 	[];
	DAC_Zones			=	[];

	//=============================================================================================================|
	
	_scr = [] spawn (compile preprocessFile "DAC\Scripts\DAC_Start_Creator.sqf");
	waituntil {scriptdone _scr};
	sleep 0.1;
	waituntil {(DAC_Basic_Value > 0)};
	
if(DAC_Code < 2) then
{
	//===========================================|
	// StartScriptOnServer                       |
	//===========================================|
	//player sidechat "ServerStart"
	//[] execVM "myServerScript.sqf";
	//onMapSingleClick "_fun = [_pos,_shift]execVM ""Action.sqf""";
}
else
{
	if(DAC_Code == 3) then
	{
		//===========================================|
		// StartScriptOnJipClient                    |
		//===========================================|
		//player sidechat "JipClientStart"
		//[] execVM "myJipClientScript.sqf";
	}
	else
	{
		//===========================================|
		// StartScriptOnClient                       |
		//===========================================|
		//player sidechat "ClientStart"
		//[] execVM "myClientScript.sqf";
		//onMapSingleClick "_fun = [_pos,_shift]execVM ""Action.sqf""";
	};
};