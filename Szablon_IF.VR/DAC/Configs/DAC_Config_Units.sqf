//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 2.1 - 2009    //
//--------------------------//
//    DAC_Config_Units      //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

private ["_TypNumber","_TempArray","_Unit_Pool_S","_Unit_Pool_V","_Unit_Pool_T","_Unit_Pool_A"];
_TypNumber = _this select 0;_TempArray = [];


/*
/////////////////////////////////////////////
///////////////SPIS TRESCI///////////////////
/////////////////////////////////////////////
/////////CIVILIANS//////////////////////////
     3 - A3
	 4 - WWII

//////////BLUFOR//////////
	 5 - Wehrmacht
	 6 - SS
	 7 - Wehrmacht [Winter]
	 8 - IJA

//////////OPFOR///////////
	 9 - ZSRR
	10 - ZSRR [WInter]
	
//////////INDFOR//////////
	11 - USA
	12 - USMC
	13 - UK
	14 - AK

*////////////////////////////////////////////

switch (_TypNumber) do
{
//-------------------------------------------------------------------------------------------------
// REDFOR CSAT (A3)
  case 0:
  {
    _Unit_Pool_S = ["O_crew_F","O_Helipilot_F","O_Soldier_SL_F","O_soldier_AR_F","O_soldier_AR_F","O_soldier_exp_F","O_soldier_GL_F","O_soldier_GL_F","O_soldier_M_F","O_medic_F","O_soldier_AA_F","O_soldier_repair_F","O_Soldier_F","O_Soldier_F","O_soldier_LAT_F","O_soldier_LAT_F","O_soldier_lite_F","O_soldier_TL_F","O_soldier_TL_F"];
    _Unit_Pool_V = ["O_MRAP_02_F","O_MRAP_02_gmg_F","O_MRAP_02_hmg_F"];
    _Unit_Pool_T = ["O_MBT_02_arty_F","O_APC_Tracked_02_cannon_F","O_APC_Wheeled_02_rcws_F","O_MBT_02_cannon_F","O_APC_Tracked_02_AA_F"];
	_Unit_Pool_A = ["O_Heli_Attack_02_F","O_Heli_Light_02_F","O_Heli_Light_02_armed_F"];
  };
//-------------------------------------------------------------------------------------------------
// BLUFOR NATO (A3)
  case 1:
  {
    _Unit_Pool_S = ["B_crew_F","B_Helipilot_F","B_Soldier_SL_F","B_soldier_AR_F","B_soldier_AR_F","B_soldier_exp_F","B_soldier_GL_F","B_soldier_GL_F","B_soldier_AA_F","B_soldier_M_F","B_medic_F","B_soldier_repair_F","B_Soldier_F","B_Soldier_F","B_soldier_LAT_F","B_soldier_LAT_F","B_soldier_lite_F","B_soldier_TL_F","B_soldier_TL_F"];
    _Unit_Pool_V = ["B_MRAP_01_F","B_MRAP_01_gmg_F","B_MRAP_01_hmg_F"];
    _Unit_Pool_T = ["B_APC_Wheeled_01_cannon_F","B_APC_Tracked_01_AA_F","B_APC_Tracked_01_rcws_F","B_MBT_01_cannon_F","B_MBT_01_arty_F","B_MBT_01_mlrs_F"];
    _Unit_Pool_A = ["B_Heli_Light_01_armed_F","B_Heli_Transport_01_camo_F","B_Heli_Light_01_F"];
  };
//-------------------------------------------------------------------------------------------------
// Independent FIA (A3)
  case 2:
  {
    _Unit_Pool_S = ["I_crew_F","I_helipilot_F","I_officer_F","I_Soldier_AT_F","I_Soldier_AA_F","I_Soldier_M_F","I_Soldier_GL_F","I_Soldier_exp_F","I_engineer_F","I_medic_F","I_Soldier_AR_F","I_Soldier_A_F"];
    _Unit_Pool_V = ["I_Truck_02_covered_F","I_Truck_02_transport_F","I_MRAP_03_hmg_F","I_MRAP_03_gmg_F","I_MRAP_03_F"];
    _Unit_Pool_T = ["I_MBT_03_cannon_F","I_APC_tracked_03_cannon_F"];
    _Unit_Pool_A = ["I_Heli_light_03_F"];
  };
//-------------------------------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////
//////////////////////CIVILIANS///////////////////////////////////
//////////////////////////////////////////////////////////////////

// Civilians (A3)
  case 3:
  {
    _Unit_Pool_S = ["C_man_1","C_man_1","C_man_1","C_man_polo_1_F","C_man_polo_2_F","C_man_polo_3_F","C_man_polo_4_F","C_man_polo_5_F","C_man_polo_6_F","C_man_1_1_F","C_man_1_2_F","C_man_1_3_F"];
    _Unit_Pool_V = ["C_Van_01_box_F","C_Van_01_transport_F","C_Offroad_01_F","C_Hatchback_01_sport_F","C_Hatchback_01_F"];
    _Unit_Pool_T = [];
    _Unit_Pool_A = [];
  };
  
//-------------------------------------------------------------------------------------------------

// Civilians (WWII)
  case 4:
  {
    _Unit_Pool_S = ["LOP_AFR_Civ_Man_01","LOP_AFR_Civ_Man_02","LOP_AFR_Civ_Man_03","LOP_AFR_Civ_Man_04","LOP_AFR_Civ_Man_05","LOP_AFR_Civ_Man_06"];
    _Unit_Pool_V = ["LOP_AFR_Civ_Landrover","LOP_AFR_Civ_Offroad","LOP_AFR_Civ_UAZ_Open","LOP_AFR_Civ_Ural_open"];
    _Unit_Pool_T = [];
    _Unit_Pool_A = [];
  };

//////////////////////////////////////////////////////////////////
//////////////////////BLUFOR//////////////////////////////////////
//////////////////////////////////////////////////////////////////

// Wehrmacht
	case 5:
	{
    _Unit_Pool_S = ["fow_s_ger_heer_tankcrew_01_shutz","LIB_GER_pilot","fow_s_ger_heer_tl_mp40","fow_s_ger_heer_medic","LIB_GER_AT_soldier","fow_s_ger_heer_mg42_asst","fow_s_ger_heer_mg42_sparebarrel","fow_s_ger_heer_rifleman_mp40","fow_s_ger_heer_mg42_gunner","fow_s_ger_heer_mg34_gunner"];
    _Unit_Pool_V = ["LIB_SdKfz251","LIB_SdKfz_7_AA","LIB_SdKfz251_FFV","LIB_Kfz1_camo","LIB_Kfz1_MG42_camo","LIB_OpelBlitz_Tent_Y_Camo","LIB_SdKfz222_camo","LIB_SdKfz234_1","LIB_SdKfz234_4","LIB_SdKfz234_3","LIB_SdKfz234_2"];
    _Unit_Pool_T = ["ifa3_pz3j","LIB_FlakPanzerIV_Wirbelwind","LIB_PzKpfwIV_H","LIB_PzKpfwV","LIB_PzKpfwVI_E","LIB_SdKfz124","LIB_StuG_III_G"];
    _Unit_Pool_A = [];
  };
  
//-------------------------------------------------------------------------------------------------
// SS
	case 6:
	{
    _Unit_Pool_S = ["fow_s_ger_heer_tankcrew_01_shutz","LIB_GER_pilot","fow_s_ger_ss_tl_stg","fow_s_ger_ss_rifleman_mp40","fow_s_ger_ss_radio_operator","fow_s_ger_ss_rifleman","fow_s_ger_ss_medic","fow_s_ger_ss_mg42_gunner","fow_s_ger_ss_mg34_gunner","fow_s_ger_ss_mg42_asst"];
    _Unit_Pool_V = ["LIB_SdKfz251","LIB_SdKfz_7_AA","LIB_SdKfz251_FFV","LIB_Kfz1_camo","LIB_Kfz1_MG42_camo","LIB_OpelBlitz_Tent_Y_Camo","LIB_SdKfz222_camo","LIB_SdKfz234_1","LIB_SdKfz234_4","LIB_SdKfz234_3","LIB_SdKfz234_2"];
    _Unit_Pool_T = ["ifa3_pz3j","LIB_FlakPanzerIV_Wirbelwind","LIB_PzKpfwIV_H","LIB_PzKpfwV","LIB_PzKpfwVI_E","LIB_SdKfz124","LIB_StuG_III_G"];
    _Unit_Pool_A = [];
  };

//-------------------------------------------------------------------------------------------------

// Wehrmacht [Wehrmacht]
  case 7:
  {
    _Unit_Pool_S = ["fow_s_ger_heer_tankcrew_01_shutz","LIB_GER_pilot","LIB_GER_Lieutenant_w","LIB_GER_Mgunner_w","LIB_GER_Radioman_w","LIB_GER_AT_soldier_w","LIB_GER_AT_grenadier_w","LIB_GER_Scout_rifleman_w","LIB_GER_Scout_ober_rifleman_w","LIB_GER_Medic_w"];
    _Unit_Pool_V = ["LIB_Sdkfz251_w","LIB_SdKfz_7_AA_w","LIB_SdKfz251_FFV_w","LIB_Kfz1_w","LIB_Kfz1_MG42_camo","LIB_OpelBlitz_Tent_Y_Camo_w","LIB_SdKfz222_camo","LIB_SdKfz234_4","LIB_SdKfz234_3","LIB_SdKfz234_1","LIB_SdKfz234_2"];
    _Unit_Pool_T = ["ifa3_pz3j","LIB_FlakPanzerIV_Wirbelwind_w","LIB_PzKpfwIV_H_w","LIB_PzKpfwV_w","LIB_PzKpfwVI_E_w","LIB_SdKfz124","LIB_StuG_III_G_w"];
    _Unit_Pool_A = [];
  };
 
//-------------------------------------------------------------------------------------------------

// IJA
  case 8:
  {
    _Unit_Pool_S = ["fow_s_ija_crewman", "fow_s_ija_pilot", "fow_s_ija_officer", "fow_s_ija_nco", "fow_s_ija_rifleman", "fow_s_ija_rifleman_at", "fow_s_ija_type99_gunner", "fow_s_ija_type99_asst"];
    _Unit_Pool_V = ["fow_v_type97_truck_ija"];
    _Unit_Pool_T = ["fow_ija_type95_HaGo_1_ija","fow_ija_type95_HaGo_2_ija"];
    _Unit_Pool_A = [];
  };

  //////////////////////////////////////////////////////////////////
//////////////////////OPFOR///////////////////////////////////////
////////////////////////////////////////////////////////////////// 
    
// ZSRR
  case 9:
  {
    _Unit_Pool_S = ["LIB_SOV_tank_crew","LIB_SOV_pilot","LIB_SOV_lieutenant","LIB_SOV_sapper","LIB_SOV_medic","LIB_SOV_smgunner","LIB_SOV_mgunner","LIB_SOV_p_officer","LIB_SOV_LC_rifleman_summer","LIB_SOV_operator","LIB_SOV_AT_grenadier"];
    _Unit_Pool_V = ["ifa3_Ba10","ifa3_ba64B","ifa3_gaz55_van","LIB_Scout_M3","LIB_Zis5v","LIB_Zis5v_61K","LIB_Willys_MB","ifa3_gazaa_IZ_max","ifa3_gazaa_IZ","ifa3_gazaa_max","ifa3_gazaa_dshk"];
    _Unit_Pool_T = ["LIB_JS2_43","LIB_T34_85","LIB_T34_76","LIB_SU85","ifa3_kv1a","ifa3_kv2","ifa3_t70m","ifa3_t60"];
    _Unit_Pool_A = [];
  }; 
  
//-------------------------------------------------------------------------------------------------
  
// ZSRR [WInter]
  case 10:
  {
    _Unit_Pool_S = ["LIB_SOV_tank_crew","LIB_SOV_pilot","LIB_SOV_First_lieutenant_w","LIB_SOV_Sapper_w","LIB_SOV_Operator_w","LIB_SOV_Mgunner_w","LIB_SOV_Smgunner_w2","LIB_SOV_Medic_w","LIB_SOV_AT_grenadier_w","LIB_SOV_P_officer_w"];
    _Unit_Pool_V = ["ifa3_Ba10","ifa3_ba64B","ifa3_gaz55_van","LIB_Scout_M3","LIB_Zis5v","LIB_Zis5v_61K","LIB_Willys_MB","ifa3_gazaa_IZ_max","ifa3_gazaa_IZ","ifa3_gazaa_max","ifa3_gazaa_dshk"];
    _Unit_Pool_T = ["LIB_JS2_43","LIB_T34_85","LIB_T34_76","LIB_SU85","ifa3_kv1a","ifa3_kv2","ifa3_t70m","ifa3_t60"];
    _Unit_Pool_A = [];
  }; 
  

//-------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////
//////////////////////INDFOR//////////////////////////////////////
//////////////////////////////////////////////////////////////////  

// USA 
  case 11:
  {
    _Unit_Pool_S = ["LIB_US_tank_crew","fow_s_us_pilot_green","fow_s_us_officer","fow_s_us_radio_operator","fow_s_us_rifleman_m1_carbine","fow_s_us_rifleman_m1903","fow_s_us_bar_gunner","fow_s_us_m1919a6_gunner","fow_s_us_engineer","fow_s_us_rifleman_scout","fow_s_us_medic","fow_s_us_at","fow_s_us_at_asst"];
    _Unit_Pool_V = ["LIB_M8_Greyhound","LIB_US_GMC_Tent","LIB_US_Scout_M3_FFV","LIB_US_Willys_MB","LIB_US_M3_Halftrack"];
    _Unit_Pool_T = ["LIB_M3A3_Stuart","LIB_M4A4_FIREFLY","LIB_M4A3_76","LIB_M4A3_76_HVSS","LIB_M5A1_Stuart","LIB_M4A3_75"];
    _Unit_Pool_A = [];
  }; 

//-------------------------------------------------------------------------------------------------
  
// USMC
  case 12:
  {
    _Unit_Pool_S = ["LIB_US_tank_crew","fow_s_us_pilot_green","fow_s_usmc_camo01_officer","fow_s_usmc_camo01_rifleman_m1903","fow_s_usmc_camo01_rifleman_m1912","fow_s_usmc_camo01_rifleman","fow_s_usmc_camo01_rifleman_m1_carbine","fow_s_usmc_camo01_m1919a6_gunner","fow_s_usmc_camo01_bar_gunner","fow_s_usmc_camo01_at","fow_s_usmc_camo01_m1919a6_asst","fow_s_usmc_camo01_bar_asst","fow_s_usmc_camo01_at_asst"];
    _Unit_Pool_V = ["LIB_M8_Greyhound","LIB_US_GMC_Tent","LIB_US_Scout_M3_FFV","LIB_US_Willys_MB","LIB_US_M3_Halftrack"];
    _Unit_Pool_T = ["LIB_M3A3_Stuart","LIB_M4A4_FIREFLY","LIB_M4A3_76","LIB_M4A3_76_HVSS","LIB_M5A1_Stuart","LIB_M4A3_75","fow_v_lvta2_usmc"];
    _Unit_Pool_A = [];
  }; 

//-------------------------------------------------------------------------------------------------
 
// UK
  case 13:
  {
    _Unit_Pool_S = ["fow_s_uk_crewman","fow_s_uk_crewman","fow_s_uk_pib_officer","fow_s_uk_pib_medic","fow_s_uk_pib_radio_operator","fow_s_uk_pib_at","fow_s_uk_pib_at_asst","fow_s_uk_pib_bren_asst","fow_s_uk_pib_bren_gunner"];
    _Unit_Pool_V = ["fow_v_universalCarrier","LIB_US_GMC_Tent","LIB_US_Scout_M3","LIB_US_Willys_MB"];
    _Unit_Pool_T = ["LIB_M4A4_FIREFLY","fow_v_cromwell_uk"];
    _Unit_Pool_A = [];
  }; 

//-------------------------------------------------------------------------------------------------
  
// AK
  case 14:
  {
    _Unit_Pool_S = ["LIB_WP_Starszy_saper","LIB_WP_Starszy_saper","LIB_WP_Sierzant","LIB_WP_Stggunner","LIB_WP_Starszy_strzelec","LIB_WP_Strzelec","LIB_WP_Medic","LIB_WP_Saper","LIB_WP_Mgunner","LIB_WP_AT_grenadier"];
    _Unit_Pool_V = ["LIB_US_Willys_MB"];
    _Unit_Pool_T = ["LIB_US_Willys_MB"];
    _Unit_Pool_A = [];
  }; 
 
//-------------------------------------------------------------------------------------------------

// XX (INDFOR)
  case 30:
  {
    _Unit_Pool_S = [];
    _Unit_Pool_V = [];
    _Unit_Pool_T = [];
    _Unit_Pool_A = [];
  }; 

//-------------------------------------------------------------------------------------------------


Default
  {
    if(DAC_Basic_Value != 5) then
    {
      DAC_Basic_Value = 5;publicvariable "DAC_Basic_Value",
      hintc "Error: DAC_Config_Units > No valid config number";
    };
    if(true) exitwith {};
  };
};

if(count _this == 2) then
{
  _TempArray = _TempArray + [_Unit_Pool_S,_Unit_Pool_V,_Unit_Pool_T,_Unit_Pool_A];
}
else
{
  _TempArray = _Unit_Pool_V + _Unit_Pool_T + _Unit_Pool_A;
};
_TempArray
