// ====================================================================================
// S.D.S Assign Gear Script - [V-1.7 | 20.04.2017]
// ====================================================================================

//	SPIS TRESCI
/*

	1.	ZSRR

	
*/
// ====================================================================================
	_loadout_faction_player = f_param_player_faction_Opfor;
// ====================================================================================

//Definicje przedmiotów

// Sprzet medyczny
_personalAidKit = "ACE_personalAidKit";		// Zestaw pierwszej pomocy
_surgicalKit = "ACE_surgicalKit";			// Zestaw do szycia ran
_AED = "adv_aceCPR_AED";					// Defibrylator
_splint = "adv_aceSplint_splint";			// Szyny do usztwniania

_bandage = "ACE_fieldDressing";				// Ogólny bandaż dla piechoty
_bandage_elastic = "ACE_elasticBandage";	// Bandaż elastyczny
_bandage_packing = "ACE_packingBandage";	//
_bandage_quikclot = "ACE_quikclot";			// Opatrunek "QuikClot"
_tourniquet ="ACE_tourniquet";				// Opaska uciskowa

_epinephrine = "ACE_epinephrine";			// Epinefryna (zwiększa tętno)
_morphine = "ACE_morphine";					// Morfina (niweluje ból, rozrzedza krew)
_adenosine = "ACE_adenosine";				// Adenozyna (obniża tętno)
_atropine = "ACE_atropine";					// Atropina (obniża tętno)

_blood_s = "ACE_bloodIV_250";				// Krew 200 ml
_blood_m = "ACE_bloodIV_500";				// Krew 500 ml
_blood_b = "ACE_bloodIV";					// Krew 1000 ml

_plasma_s = "ACE_plasmaIV_250";				// Osocze 200 ml
_plasma_m = "ACE_plasmaIV_500";				// Osocze 500 ml
_plasma_b = "ACE_plasmaIV";					// Osocze 1000 ml

_saline_s = "ACE_salineIV_250";				// Sól fizjologiczna 200 ml
_saline_m = "ACE_salineIV_500";				// Sól fizjologiczna 500 ml
_saline_b = "ACE_salineIV";					// Sól fizjologiczna 1000 ml

_firstaid = "FirstAidKit";
_medkit = "Medikit";

//Radia ACRE2
_radioSR = "ACRE_PRC343";
_radioMR = "ACRE_PRC152";
_radioLR = "ACRE_PRC77";

//Przydatne rzeczy
_earplugs = "ACE_EarPlugs";
_IRstrobe = "ACE_IR_Strobe_Item";
_latarka = "ACE_Flashlight_XL50";
_lornetkaAM = "ACE_Vector";
_lornetkaFTL = "Binocular";
_lornetkaRTO = "Binocular";
_GPS = "ItemGPS";
_handcuffs = "ACE_CableTie";
_worek = "ACE_bodyBag";
_huntIR = "ACE_HuntIR_M203";
_huntIR_tab = "ACE_HuntIR_monitor";
_wissle = "fow_i_whistle";

// Flary ręczne
_flarewhite = "ACE_HandFlare_White";
_flarered = "ACE_HandFlare_Red";
_flareyellow = "ACE_HandFlare_Yellow";
_flaregreen = "ACE_HandFlare_Green";

/*
// Świetliki
_chemgreen =  "Chemlight_green";
_chemred = "Chemlight_red";
_chemyellow =  "Chemlight_yellow";
_chemblue = "Chemlight_blue";
*/

//Granaty dymne
_smokegrenade = "SmokeShell";
_smokegrenadegreen = "SmokeShellGreen";
_smokegrenadeblue = "SmokeShellBlue";
_smokegrenadered = "SmokeShellRed";

// Granaty ręczne
_grenade = "LIB_F1";
_mgrenade = "ACE_M84";

/*
// Granaty dymne do granatnika
_glsmokewhite = "1Rnd_Smoke_Grenade_shell";
_glsmokegreen = "1Rnd_SmokeGreen_Grenade_shell";
_glsmokered = "1Rnd_SmokeRed_Grenade_shell";

// Flary do granatnika
_glflarewhite = "CUP_1Rnd_StarFlare_White_M203";
_glflarered = "CUP_1Rnd_StarFlare_Red_M203";
_glflareyellow = "UGL_FlareYellow_F";
_glflaregreen = "CUP_1Rnd_StarFlare_Green_M203";
*/

//Przedmioty w skrzyniach
_satche_small = "LIB_Ladung_Small_MINE_mag";
_satche_big = "LIB_Ladung_Big_MINE_mag";
_satche_extra_big = "SatchelCharge_Remote_Mag";
_toolkit = "ToolKit";
_zapalnik = "ACE_LIB_FireCord";
_zapalnik_b = "ACE_LIB_LadungPM";
_n_rozbraja = "ACE_DefusalKit";
_saperka = "ACE_EntrenchingTool";
_mlotek = "ACE_Fortify";
_wirecutter = "ACE_wirecutter";

// Mechanicy/saperzy
_ATmine = "ATMine_Range_Mag";
_APmine1 = "APERSBoundingMine_Range_Mag";
_APmine2 = "APERSMine_Range_Mag";
_mineDetector = "ACE_VMM3";

// Noktowizja
_nvg = "CUP_NVG_PVS15_black";

// Terminal UAV
_uavterminal = "B_UavTerminal";	  // Dla BLUFORU musi być terminal BLUFORU, innej frakcji nie zadziała

//Konstruktor BUILDERA
Builder ={	
	_backpack = {
		_typeofBackPack = _this select 0;
		_loadout = f_param_backpacks;
		if (count _this > 1) then {_loadout = _this select 1};
		switch (_typeofBackPack) do
		{
			#include "f_assignGear_csat_b.sqf";
		};
	};
// SETUP CRATE
	_crate = {
		_typeofCrate = _this select 0;
		switch (_typeofCrate) do
		{
			#include "f_assignGear_crate.sqf";
		};
	};
// WYWOŁANIE BUILDERA
		#include "f_assignGear_csat_builder.sqf";
};
	
//Dodanie wyposażenia medycznego
Medical = {
	for "_p" from 1 to 4 do {_unit addItem _bandage;};		// Dodanie 8 sztuk bandaży	
	_unit linkItem "ItemMap";		// Dodanie mapy
	_unit linkItem "ItemCompass";	// Dodanie kompsu
	_unit linkItem "ItemWatch";		// Dodanie zegarka
	_unit addItem _earplugs;		// Dodaje zatyczki do uszy
	_unit addItem _latarka;			// Latarka na mape
};

//Nocne wyposażenie oparte na noktowizji, laserach, flarach
Night_Eq_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
			_unit addItem _IRstrobe;		// Znacznik IR (doczepiany)
			_unit addItem _IRstrobe;		
			(unitBackpack _unit) addItemCargoGlobal [_chemblue,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarewhite,4];
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarered,4];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji	
		};
	};
};

//Nocne wyposażenie oparte na latarkach, flarach, lightstick-ach
Night_Eq_No_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
		
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_flarered,2];
		(unitBackpack _unit) addMagazineCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
		};
	};
};

// ====================================================================================

switch (_loadout_faction_player) do
{

	//ZSRR
	case 1: 
	{
_attach1 = "rhs_acc_2dpZenit_ris";		// Latarka (RHS)

_silencer1 = "LIB_ACC_M1891_Bayo";				// Tłumik płomieni (domyslny do wszytskich AK) (RHS)

_scope1 = "optic_ACO_grn";				// Celownik holo (RHS)

_bipod1 = "rhs_acc_grip_ffg2";			// Default bipod

// Jakie dodatki mają być dodane
_attachments = [_silencer1]; 	// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];

// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "LIB_M9130";
_riflemag = "LIB_5Rnd_762x54";
_riflemag_tr = "LIB_5Rnd_762x54_t46";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "LIB_SVT_40";
_carbinemag = "LIB_10Rnd_762x54";
_carbinemag_tr = "LIB_10Rnd_762x54_t46";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
_smg = "IFA3_PPS43";
_smgmag = "IFA3_35Rnd_762x25_PPS";
_smgmag_tr = "IFA3_35Rnd_762x25_PPS_t";

// Broń z granatnikiem (dla dowóców)
_glrifle = "LIB_PPSh41_m";
_glriflemag = "LIB_71Rnd_762x25";
_glriflemag_tr = "LIB_71Rnd_762x25_t";
//_glmag = "rhs_VOG25";

// Pistolet (dla wszystkich klas)
_pistol = "LIB_TT33";
_pistolmag = "LIB_8Rnd_762x25";

/*
// Siły specjalne
_diverWepCaS = "arifle_min_rf_ak12_camo_grip";
_diverMagCaS = "30Rnd_min_rf_545x39_mag";
_diverWepR = "arifle_min_rf_ak12_camo_grip";
_diverMagR = "30Rnd_min_rf_545x39_mag";
_diverWepM = "arifle_min_rf_ak12_camo";
_diverMagM = "30Rnd_min_rf_545x39_mag";
*/

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "LIB_DP28";
_ARmag = "LIB_47Rnd_762x54";
_ARmag_tr = "LIB_47Rnd_762x54d";

/*
// Strzelec MMG
_MMG = "LIB_DT";
_MMGmag = "LIB_63Rnd_762x54";
_MMGmag_tr = "LIB_63Rnd_762x54";
*/

// Strzelec wyborowy
_DMrifle = "LIB_M9130PU";
_DMriflemag = "LIB_5Rnd_762x54";

/*
// Strzelec AT
_RAT = "rhs_weap_rpg26";
_RATmag = "rhs_rpg26_mag";
*/

// Strzelec MAT
_MAT = "LIB_PTRD";
_MATmag1 = "LIB_1Rnd_145x114";
_MATmag2 = "LIB_1Rnd_145x114_T";
//_MAT_sight = "rhs_acc_pgo7v";

/*
// Strzelec AA
_SAM = "rhs_weap_igla";
_SAMmag = "rhs_mag_9k38_rocket";

// Strzelec HAT
_HAT = "launch_O_Titan_short_F";
_HATmag1 = "Titan_AT";
_HATmag2 = "Titan_AP";

// Snajper
_SNrifle = "srifle_GM6_F";
_SNrifleMag = "5Rnd_127x108_Mag";
*/

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";		         		// Parachute

_bagFTL = "B_LIB_SOV_RA_GasBag";                  	// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_SOV_RA_Radio";                  		// Plecak dla pilota (radio)

_bagTL = "B_LIB_SOV_RA_Shinel";						//Plecak dowódcy drużyny

_bagMs = "B_LIB_SOV_RA_MedicalBag_Empty";					// Plecak dla medyka (mały)
_bagMm = "B_LIB_SOV_RA_MedicalBag_Empty";						// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_khk";               	// Plecak dla medyka (duży)

_bagARs = "B_LIB_SOV_RA_Rucksack2";					// Plecak dla RKM (mały)
_bagARm = "B_LIB_SOV_RA_Rucksack2";						// Plecak dla RKM (średni)
_bagARb = "B_Carryall_oli";                     // Plecak dla RKM (duży)

_bagENG = "B_LIB_SOV_RA_Rucksack2";	// Plecak dla Mechanika

_bagR = "B_LIB_SOV_RA_Shinel";						// Plecak dla strzelca, strzelca AT

_bagMAT = "B_FieldPack_khk";						// Plecak dla MAT

_bagmedium = "B_Kitbag_sgg";					// carries 200, weighs 30
_baglarge =  "B_Carryall_oli"; 					// carries 320, weighs 40

_bagmediumdiver =  "rhs_assault_umbts";			// Plecaki SF

_baguav = "B_LIB_SOV_RA_Radio";						// Plecak RTO

_baghmgg = "LIB_Maxim_Bar";					// used by Heavy MG gunner
_baghmgag = "LIB_Maxim_Bag";				// used by Heavy MG assistant gunner

_baghatg = "O_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "O_HMG_01_support_F";				// used by Heavy AT assistant gunner

_bagmtrg = "LIB_BM37_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_BM37_Bag";			// used by Mortar assistant gunner

_baghsamg = "O_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "O_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = ["m"];
_heavy =  ["eng","engm" ,"aar" , "ar"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["U_LIB_SOV_Strelok_summer"];
_baseHelmet = ["H_LIB_SOV_RA_PrivateCap","H_LIB_SOV_RA_Helmet"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["V_LIB_SOV_RA_MosinBelt","V_LIB_SOV_RA_MGBelt","V_LIB_SOV_RA_PPShBelt","V_LIB_SOV_RA_SVTBelt"];
_mediumRig = ["V_LIB_SOV_RA_MosinBelt","V_LIB_SOV_RA_MGBelt","V_LIB_SOV_RA_PPShBelt","V_LIB_SOV_RA_SVTBelt"];
_heavyRig = ["V_LIB_SOV_RA_MosinBelt","V_LIB_SOV_RA_MGBelt","V_LIB_SOV_RA_PPShBelt","V_LIB_SOV_RA_SVTBelt"];

/*
// Siły specjalne
_diverUniform =  ["rhs_uniform_gorka_r_y"];
_diverHelmet = ["rhs_ssh68"];
_diverRig = ["rhs_vydra_3m"];
_diverGlasses = [];
*/

// Pilot
_pilotUniform = ["U_LIB_SOV_Pilot"];
_pilotHelmet = ["H_LIB_SOV_PilotHelmet"];
_pilotRig = ["V_LIB_SOV_RA_Belt"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["U_LIB_SOV_Tank_ryadovoi"];
_crewHelmet = ["H_LIB_SOV_TankHelmet"];
_crewRig = ["V_LIB_SOV_RA_Belt"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_O_GhillieSuit"]; 
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_khk"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["rhs_uniform_df15_tan"];
_sfhelmet = ["rhs_zsh7a_alt"];
_sfRig = ["V_Rangemaster_belt"];
_sfGlasses = [];

// Medyk
_medUniform =  ["U_LIB_SOV_Strelok_summer"];
_medHelmet = ["H_LIB_SOV_RA_Helmet"];
_medRig = ["V_LIB_SOV_RA_MosinBelt"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["U_LIB_SOV_Kapitan_summer"];
_dowHelmet = ["H_LIB_SOV_RA_OfficerCap"];
_dowRig = ["V_LIB_SOV_RA_TankOfficerSet"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)

	call Medical;

};

// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;
	
	//WYWOŁANIE BUILDERA
	call Builder;
		
};
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	
	
// ====================================================================================
// ====================================================================================
//EMD CASE STATEMENT		

};

// ====================================================================================
// ====================================================================================
// If this isn't run on an infantry unit we can exit
if !(_isMan) exitWith {};

// ====================================================================================
// Handle weapon attachments
#include "f_assignGear_attachments.sqf";

// ====================================================================================
// ENSURE UNIT HAS CORRECT WEAPON SELECTED ON SPAWNING
_unit selectweapon primaryweapon _unit;