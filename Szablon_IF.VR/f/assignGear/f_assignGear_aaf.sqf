// ====================================================================================
// S.D.S Assign Gear Script - [V-1.8 beta | 04.01.2018]
// ====================================================================================

//	SPIS TRESCI
/*

	1.	US Para

	
*/
// ====================================================================================
	_loadout_faction_player = f_param_player_faction_Indfor;
// ====================================================================================

//Definicje przedmiotów

// Sprzet medyczny
_personalAidKit = "ACE_personalAidKit";		// Zestaw pierwszej pomocy
_surgicalKit = "ACE_surgicalKit";			// Zestaw do szycia ran
_AED = "adv_aceCPR_AED";					// Defibrylator
_splint = "adv_aceSplint_splint";			// Szyny do usztwniania

_bandage = "ACE_fieldDressing";				// Ogólny bandaż dla piechoty
_bandage_elastic = "ACE_elasticBandage";	// Bandaż elastyczny
_bandage_packing = "ACE_packingBandage";	//
_bandage_quikclot = "ACE_quikclot";			// Opatrunek "QuikClot"
_tourniquet ="ACE_tourniquet";				// Opaska uciskowa

_epinephrine = "ACE_epinephrine";			// Epinefryna (zwiększa tętno)
_morphine = "ACE_morphine";					// Morfina (niweluje ból, rozrzedza krew)
_adenosine = "ACE_adenosine";				// Adenozyna (obniża tętno)
_atropine = "ACE_atropine";					// Atropina (obniża tętno)

_blood_s = "ACE_bloodIV_250";				// Krew 200 ml
_blood_m = "ACE_bloodIV_500";				// Krew 500 ml
_blood_b = "ACE_bloodIV";					// Krew 1000 ml

_plasma_s = "ACE_plasmaIV_250";				// Osocze 200 ml
_plasma_m = "ACE_plasmaIV_500";				// Osocze 500 ml
_plasma_b = "ACE_plasmaIV";					// Osocze 1000 ml

_saline_s = "ACE_salineIV_250";				// Sól fizjologiczna 200 ml
_saline_m = "ACE_salineIV_500";				// Sól fizjologiczna 500 ml
_saline_b = "ACE_salineIV";					// Sól fizjologiczna 1000 ml

_firstaid = "FirstAidKit";
_medkit = "Medikit";

//Radia ACRE2
_radioSR = "ACRE_PRC343";
_radioMR = "ACRE_PRC152";
_radioLR = "ACRE_PRC77";

//Przydatne rzeczy
_earplugs = "ACE_EarPlugs";
_IRstrobe = "ACE_IR_Strobe_Item";
_latarka = "ACE_Flashlight_XL50";
_lornetkaAM = "ACE_Vector";
_lornetkaFTL = "Binocular";
_lornetkaRTO = "Binocular";
_GPS = "ItemGPS";
_handcuffs = "ACE_CableTie";
_worek = "ACE_bodyBag";
_huntIR = "ACE_HuntIR_M203";
_huntIR_tab = "ACE_HuntIR_monitor";
_wissle = "fow_i_whistle";

// Flary ręczne
_flarewhite = "ACE_HandFlare_White";
_flarered = "ACE_HandFlare_Red";
_flareyellow = "ACE_HandFlare_Yellow";
_flaregreen = "ACE_HandFlare_Green";

/*
// Świetliki
_chemgreen =  "Chemlight_green";
_chemred = "Chemlight_red";
_chemyellow =  "Chemlight_yellow";
_chemblue = "Chemlight_blue";
*/

//Granaty dymne
_smokegrenade = "LIB_US_M18";
_smokegrenadegreen = "SmokeShellGreen";
_smokegrenadeblue = "SmokeShellBlue";
_smokegrenadered = "SmokeShellRed";

// Granaty ręczne
_grenade = "LIB_US_Mk_2";
_mgrenade = "ACE_M84";

/*
// Granaty dymne do granatnika
_glsmokewhite = "1Rnd_Smoke_Grenade_shell";
_glsmokegreen = "1Rnd_SmokeGreen_Grenade_shell";
_glsmokered = "1Rnd_SmokeRed_Grenade_shell";

// Flary do granatnika
_glflarewhite = "CUP_1Rnd_StarFlare_White_M203";
_glflarered = "CUP_1Rnd_StarFlare_Red_M203";
_glflareyellow = "UGL_FlareYellow_F";
_glflaregreen = "CUP_1Rnd_StarFlare_Green_M203";
*/

//Przedmioty w skrzyniach
_satche_small = "LIB_Ladung_Small_MINE_mag";
_satche_big = "LIB_Ladung_Big_MINE_mag";
_satche_extra_big = "SatchelCharge_Remote_Mag";
_toolkit = "ToolKit";
_zapalnik = "ACE_LIB_FireCord";
_zapalnik_b = "ACE_LIB_LadungPM";
_n_rozbraja = "ACE_DefusalKit";
_saperka = "ACE_EntrenchingTool";
_mlotek = "ACE_Fortify";
_wirecutter = "ACE_wirecutter";

// Mechanicy/saperzy
_ATmine = "ATMine_Range_Mag";
_APmine1 = "APERSBoundingMine_Range_Mag";
_APmine2 = "APERSMine_Range_Mag";
_mineDetector = "ACE_VMM3";

// Noktowizja
_nvg = "CUP_NVG_PVS15_black";

// Terminal UAV
_uavterminal = "B_UavTerminal";	  // Dla BLUFORU musi być terminal BLUFORU, innej frakcji nie zadziała

//Konstruktor BUILDERA
Builder ={	
	_backpack = {
		_typeofBackPack = _this select 0;
		_loadout = f_param_backpacks;
		if (count _this > 1) then {_loadout = _this select 1};
		switch (_typeofBackPack) do
		{
			#include "f_assignGear_aaf_b.sqf";
		};
	};
// SETUP CRATE
	_crate = {
		_typeofCrate = _this select 0;
		switch (_typeofCrate) do
		{
			#include "f_assignGear_crate.sqf";
		};
	};
// WYWOŁANIE BUILDERA
		#include "f_assignGear_aaf_builder.sqf";
	};

//Dodanie wyposażenia medycznego
Medical = {
	for "_p" from 1 to 4 do {_unit addItem _bandage;};		// Dodanie 8 sztuk bandaży	
	_unit linkItem "ItemMap";		// Dodanie mapy
	_unit linkItem "ItemCompass";	// Dodanie kompsu
	_unit linkItem "ItemWatch";		// Dodanie zegarka
	_unit addItem _earplugs;		// Dodaje zatyczki do uszy
	_unit addItem _latarka;			// Latarka na mape
};

//Nocne wyposażenie oparte na noktowizji, laserach, flarach
Night_Eq_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
			_unit addItem _IRstrobe;		// Znacznik IR (doczepiany)
			_unit addItem _IRstrobe;		
			(unitBackpack _unit) addItemCargoGlobal [_chemblue,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarewhite,4];
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarered,4];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji	
		};
	};
};

//Nocne wyposażenie oparte na latarkach, flarach, lightstick-ach
Night_Eq_No_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
		
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_flarered,2];
		(unitBackpack _unit) addMagazineCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
		};
	};
};

// ====================================================================================

switch (_loadout_faction_player) do
{
	//US Army
	case 1: 
	{
	// Dodatki broń główna
_attach1 = "rhs_acc_2dpZenit";			// Dodatek do broni

_silencer1 = "LIB_ACC_M1_Bayo";				// Bagnet

_scope1 = "rhs_acc_1p63";				// Celownik holo (RHS)

_bipod1 = "bipod_02_F_hex";				// Default bipod

// Jakie dodatki mają być dodane
_attachments = [_silencer1]; 	// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_silencer2 = "muzzle_snds_L";	// SF Pistol suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];

// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "LIB_M1_Garand";
_riflemag = "LIB_8Rnd_762x63";
_riflemag_tr = "LIB_8Rnd_762x63_t";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "LIB_M1903A3_Springfield";
_carbinemag = "LIB_5Rnd_762x63";
_carbinemag_tr = "LIB_5Rnd_762x63_t";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
_smg = "LIB_M3_GreaseGun";
_smgmag = "LIB_30Rnd_M3_GreaseGun_45ACP";
_smgmag_tr = "LIB_30Rnd_M3_GreaseGun_45ACP";

// Broń z granatnikiem (dla dowóców)
_glrifle = "LIB_M1A1_Thompson";
_glriflemag = "LIB_30Rnd_45ACP";
_glriflemag_tr = "LIB_30Rnd_45ACP_t";
//_glmag = "rhs_VOG25";

// Pistolet (dla wszystkich klas)
_pistol = "LIB_Colt_M1911";
_pistolmag = "LIB_7Rnd_45ACP";

/*
// Siły specjalne
_diverWepCaS = "arifle_min_rf_ak12_camo_grip";
_diverMagCaS = "30Rnd_min_rf_545x39_mag";
_diverWepR = "arifle_min_rf_ak12_camo_grip";
_diverMagR = "30Rnd_min_rf_545x39_mag";
_diverWepM = "arifle_min_rf_ak12_camo";
_diverMagM = "30Rnd_min_rf_545x39_mag";
*/

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "LIB_M1918A2_BAR";
_ARmag = "LIB_20Rnd_762x63";
_ARmag_tr = "LIB_20Rnd_762x63";

// Strzelec MMG
_MMG = "LIB_M1919A6";
_MMGmag = "LIB_50Rnd_762x63";
_MMGmag_tr = "LIB_50Rnd_762x63";

// Strzelec wyborowy
_DMrifle = "LIB_M1903A4_Springfield";
_DMriflemag = "LIB_5Rnd_762x63";

/*
// Strzelec AT
_RAT = "rhs_weap_rpg26";
_RATmag = "rhs_rpg26_mag";
*/

// Strzelec MAT
_MAT = "LIB_M1A1_Bazooka";
_MATmag1 = "LIB_1Rnd_60mm_M6";
_MATmag2 = "LIB_1Rnd_60mm_M6";
//_MAT_sight = "rhs_acc_pgo7v";

/*
// Strzelec AA
_SAM = "rhs_weap_igla";
_SAMmag = "rhs_mag_9k38_rocket";

// Strzelec HAT
_HAT = "launch_O_Titan_short_F";
_HATmag1 = "Titan_AT";
_HATmag2 = "Titan_AP";

// Snajper
_SNrifle = "srifle_GM6_F";
_SNrifleMag = "5Rnd_127x108_Mag";
*/

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";		         		// Parachute

_bagFTL = "B_LIB_US_M36_Rope";                  	// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_US_M36";                  		// Plecak dla pilota (radio)

_bagTL = "B_LIB_US_M36_Rope";					//Plecak dowódcy drużyny

_bagMs = "B_FieldPack_khk";							// Plecak dla medyka (mały)
_bagMm = "B_FieldPack_khk";						// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_khk";               		// Plecak dla medyka (duży)

_bagARs = "B_LIB_US_M36_Rope";					// Plecak dla RKM (mały)
_bagARm = "B_LIB_US_M36_Rope";					// Plecak dla RKM (średni)
_bagARb = "B_Carryall_oli";						// Plecak dla RKM (duży)

_bagENG = "B_LIB_US_M36";	// Plecak dla Mechanika

_bagR = "B_LIB_US_M36";							// Plecak dla strzelca, strzelca AT

_bagMAT = "B_FieldPack_khk";						// Plecak dla MAT

_bagmedium = "rhs_sidorMG";						// carries 200, weighs 30
_baglarge =  "B_Carryall_cbr"; 					// carries 320, weighs 40

_bagmediumdiver =  "B_Carryall_cbr";			// Plecaki SF

_baguav = "B_LIB_US_M36_Rope";						// Plecak RTO

_baghmgg = "RHS_Kord_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_Kord_Tripod_Bag";				// used by Heavy MG assistant gunner

_baghatg = "O_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "O_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "fow_b_us_m2_mortar_weapon";				// used by Mortar gunner
_bagmtrag = "fow_b_us_m2_mortar_support";				// used by Mortar assistant gunner
_baghsamg = "O_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "O_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm" ,"aar" , "ar" ];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = [ "ps"];
_med = ["m"];
_dow = ["co","dc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["fow_u_us_m42_ab_01_101_private"];
_baseHelmet = ["H_LIB_US_AB_Helmet_2","H_LIB_US_AB_Helmet","H_LIB_US_AB_Helmet_Clear_1","H_LIB_US_AB_Helmet_Plain_3","H_LIB_US_AB_Helmet_Plain_2","H_LIB_US_AB_Helmet_Jump_1","H_LIB_US_AB_Helmet_Jump_2"];
_baseGlasses = [];

// Kamizelki
_lightRig = [];
_mediumRig = ["fow_v_us_ab_grenade","fow_v_us_ab_carbine","fow_v_us_ab_carbine_eng","fow_v_us_ab_garand","fow_v_us_ab_bar","fow_v_us_ab_asst_mg"]; 
_heavyRig = ["fow_v_us_ab_grenade","fow_v_us_ab_carbine","fow_v_us_ab_carbine_eng","fow_v_us_ab_garand","fow_v_us_ab_bar","fow_v_us_ab_asst_mg"];

/*
// Siły specjalne
_diverUniform =  ["rhs_uniform_gorka_r_y"];
_diverHelmet = ["rhs_ssh68"];
_diverRig = ["rhs_vydra_3m"];
_diverGlasses = [];
*/

// Pilot
_pilotUniform = ["U_LIB_US_Private"];
_pilotHelmet = ["H_LIB_US_Helmet_Pilot"];
_pilotRig = ["fow_v_us_45"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["U_LIB_US_Tank_Crew"];
_crewHelmet = ["H_LIB_US_Helmet_Tank"];
_crewRig = ["fow_v_us_45"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_O_GhillieSuit"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_khk"];
_ghillieGlasses = [];

// Jet Pilot
_sfuniform = ["U_LIB_US_Private"];
_sfhelmet = ["H_LIB_US_Helmet_Pilot"];
_sfRig = ["fow_v_us_45"];
_sfGlasses = [];

// Medyk
_medUniform =  ["fow_u_us_m42_ab_01_101_flag_private"];
_medHelmet = ["H_LIB_US_AB_Helmet_Medic_1"];
_medRig = ["fow_v_us_ab_carbine"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["fow_u_us_m42_ab_01_101_flag_private"];
_dowHelmet = ["H_LIB_US_AB_Helmet_CO_2","H_LIB_US_AB_Helmet_CO_1"];
_dowRig = ["fow_v_us_ab_thompson_nco"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";
		
// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)
	call Medical;

	};

// ====================================================================================	
	//WYWOŁANIE BUILDERA
	call Builder;
	
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;
		
};
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************


	
// ====================================================================================
// ====================================================================================
//END CASE STATEMENT		

};

// ====================================================================================
// ====================================================================================
// If this isn't run on an infantry unit we can exit
if !(_isMan) exitWith {};

// ====================================================================================
// Handle weapon attachments
#include "f_assignGear_attachments.sqf";

// ====================================================================================
// ENSURE UNIT HAS CORRECT WEAPON SELECTED ON SPAWNING
_unit selectweapon primaryweapon _unit;