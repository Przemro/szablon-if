// ====================================================================================
// S.D.S Assign Gear Script - [V-1.7 | 20.04.2017]
// ====================================================================================

//	SPIS TRESCI
/*

	1.	Wehrmacht
	2.	SS
	3.	Fallschirmjäger
	4.	Wehrmacht - Winter
	5.	JIA WIP
	6.	Polska Woodland
	7.	US Desert [CW]
	8.	US Woodland [CW]
	9.	M.S.K.E Defense Forces [U] ===[WIP]===
	
*/
// ====================================================================================
	_loadout_faction_player = f_param_player_faction_Blufor;
// ====================================================================================

//Definicje przedmiotów

// Sprzet medyczny
_personalAidKit = "ACE_personalAidKit";		// Zestaw pierwszej pomocy
_surgicalKit = "ACE_surgicalKit";			// Zestaw do szycia ran
_bandage = "ACE_elasticBandage";					// Opatrunek dla piechoty
_epinephrine = "ACE_epinephrine";			// Epinefryna
_morphine = "ACE_morphine";					// Morfina
_blood = "ACE_bloodIV_500";					// Krew 500 ml
_plasma = "ACE_plasmaIV_500";				// Osocze 500 ml
_saline = "ACE_salineIV_500";				// Sól fizjologiczna 500 ml
_firstaid = "FirstAidKit";
_medkit = "Medikit";

//Przydatne rzeczy
_earplugs = "ACE_EarPlugs";
_IRstrobe = "ACE_IR_Strobe_Item";
_latarka = "ACE_Flashlight_XL50";
_lornetkaAM = "ACE_Vector";
_lornetkaFTL = "fow_i_dienstglas";
_lornetkaRTO = "fow_i_dienstglas";
//_GPS = "ItemGPS";
//_Antena_RTO = "tfw_rf3080Item";

// Flary ręczne
_flarewhite = "ACE_HandFlare_White";
_flarered = "ACE_HandFlare_Red";
_flareyellow = "ACE_HandFlare_Yellow";
_flaregreen = "ACE_HandFlare_Green";

// Świetliki
//_chemgreen =  "Chemlight_green";
//_chemred = "Chemlight_red";
//_chemyellow =  "Chemlight_yellow";
//_chemblue = "Chemlight_blue";

//Granaty dymne
_smokegrenade = "LIB_nb39";
_smokegrenadegreen = "LIB_nb39";
_smokegrenadeblue = "LIB_nb39";
_smokegrenadered = "LIB_nb39";

// Granaty ręczne
_grenade = "LIB_shg24";
_mgrenade = "ACE_M84";

// Granaty dymne do granatnika
//_glsmokewhite = "1Rnd_Smoke_Grenade_shell";
//_glsmokegreen = "1Rnd_SmokeGreen_Grenade_shell";
//_glsmokered = "1Rnd_SmokeRed_Grenade_shell";

// Flary do granatnika
//_glflarewhite = "UGL_FlareWhite_F";
//_glflarered = "UGL_FlareRed_F";
//_glflareyellow = "UGL_FlareYellow_F";
//_glflaregreen = "UGL_FlareGreen_F";

//Przedmioty w skrzyniach
_satche_small = "DemoCharge_Remote_Mag";
_satche_big = "SatchelCharge_Remote_Mag";
_toolkit = "ToolKit";
_zapalnik = "ACE_LIB_LadungPM";
_n_rozbraja = "ACE_DefusalKit";
_saperka = "ACE_EntrenchingTool";
_wirecutter = "ACE_wirecutter";

// Mechanicy/saperzy
_ATmine = "ATMine_Range_Mag";
_APmine1 = "APERSBoundingMine_Range_Mag";
_APmine2 = "APERSMine_Range_Mag";

// Noktowizja
_nvg = "rhsusf_ANPVS_15";

// Terminal UAV
_uavterminal = "B_UavTerminal";	  // Dla BLUFORU musi być terminal BLUFORU, innej frakcji nie zadziała

//Konstruktor BUILDERA
Builder = {	
	_backpack = {
		_typeofBackPack = _this select 0;
		_loadout = f_param_backpacks;
		if (count _this > 1) then {_loadout = _this select 1};
		switch (_typeofBackPack) do
		{
			#include "f_assignGear_nato_b.sqf";
		};
	};
// SETUP CRATE
	_crate = {
		_typeofCrate = _this select 0;
		switch (_typeofCrate) do
		{
			#include "f_assignGear_crate.sqf";
		};
	};
// WYWOŁANIE BUILDERA
		#include "f_assignGear_nato_builder.sqf";
};	

//Dodanie wyposażenia medycznego	
Medical = {
	for "_p" from 1 to 2 do {_unit addItem _bandage;};		// Dodanie 12 sztuk bandaży
	_unit linkItem "ItemMap";		// Dodanie mapy
	_unit linkItem "ItemCompass";	// Dodanie kompsu
	_unit linkItem "ItemRadio";		// Dodanie radia (nie z taskforce)
	_unit linkItem "ItemWatch";	// Dodanie zegarka
	_unit addItem _earplugs;		// Dodaje zatyczki do uszy
	_unit addItem _latarka;			// Latarka na mape
};

//Nocne wyposażenie oparte na noktowizji, laserach, flarach
Night_Eq_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
			_unit addItem _IRstrobe;		// Znacznik IR (doczepiany)
			_unit addItem _IRstrobe;		
			(unitBackpack _unit) addItemCargoGlobal [_chemblue,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarewhite,4];
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarered,4];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji	
		};
	};
};

//Nocne wyposażenie oparte na latarkach, flarach, lightstick-ach
Night_Eq_No_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
			_unit addItem _IRstrobe;		// Znacznik IR (doczepiany)
			_unit addItem _IRstrobe;		
			(unitBackpack _unit) addItemCargoGlobal [_chemblue,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarewhite,4];
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarered,4];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji	
		};
	};
};
	

// ====================================================================================

switch (_loadout_faction_player) do
{
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	//Wehrmacht
	case 1: 
	{
// Dodatki broń główna
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)
_attach2 = "rhsusf_acc_anpeq15_bk_light";	// Laser i latarka (można przełączyć) (RHS)
_attach3 = "rhsusf_acc_M952V";				// Latarka (RHS)

_silencer1 = "rhsusf_acc_nt4_black";		// Tłumik 5.56 (długi) (RHS)
_silencer2 = "rhsusf_acc_rotex5_tan";		// Tłumik 5.56 (krótki) (RHS)

_scope1 = "PSZ_AccO_EOT552";				// Eotech (Czarny PSZ)
_scope2 = "rhsusf_acc_compm4";				// Aimpoint (RHS)
_scope3 = "rhsusf_acc_LEUPOLDMK4_2";		// Celownik snajperski 6.5-20x

_bipod1 = "rhsusf_acc_harris_bipod";		// 
_bipod2 = "bipod_02_F_blk";					// 

// Jakie dodatki mają być dodane
_loadout_night_day = f_param_night_day_wyp;
if (_loadout_night_day == 0) then {			// Każda jednostka otrzyma ten zestaw dodatków
	_attachments = [_attach1,_scope1];		//NOC
} else {
	_attachments = [_attach3,_scope1];		//DZIEŃ 			
};

// [] = brak dodatków
// [_attach1,_scope1] = usuwa dodatki a na ich miejsce dodaje _attach1, _scope1
// [_scope2] = dodaje tylko _scope2, reszta zostaje usunięta
// false = nie zmieniaj dodatków, zostaw tak jak domyślnie arma respi na broni

// ====================================================================================
// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];
// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "fow_w_g43";
_riflemag = "fow_10nd_792x57";
_riflemag_tr = "fow_10nd_792x57";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "fow_w_k98";
_carbinemag = "fow_5Rnd_792x57";
_carbinemag_tr = "fow_5Rnd_792x57";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
//_smg = "rhsusf_weap_MP7A1_base_f";
//_smgmag = "rhsusf_mag_40Rnd_46x30_FMJ";
//_smgmag_tr = "rhsusf_mag_40Rnd_46x30_FMJ";

// Broń z granatnikiem (dla dowóców)
_glrifle = "fow_w_mp40";
_glriflemag = "fow_32Rnd_9x19_mp40";
_glriflemag_tr = "fow_32Rnd_9x19_mp40";
//_glmag = "1Rnd_HE_Grenade_shell";

// Pistolet (dla wszystkich klas)
_pistol = "fow_w_p08";
_pistolmag = "fow_8Rnd_9x19";

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "fow_w_mg42";
_ARmag = "fow_50Rnd_792x57";
_ARmag_tr = "fow_50Rnd_792x57";

// Strzelec MMG
//_MMG = "rhs_weap_m240B";
//_MMGmag = "rhsusf_100Rnd_762x51";
//_MMGmag_tr = "rhsusf_100Rnd_762x51_m62_tracer";

// Strzelec wyborowy (broń DLC)
//_DMrifle = "srifle_DMR_06_camo_F";
//_DMriflemag = "20Rnd_762x51_Mag";

// Strzelec AT
_RAT = "LIB_PzFaust_30m";
//_RATmag = "";

// Strzelec MAT
_MAT = "LIB_RPzB";
_MATmag1 = "LIB_1Rnd_RPzB";
_MATmag2 = "LIB_1Rnd_RPzB";
//_MAT_sight = "rhs_weap_optic_smaw";

// Strzelec AA
_SAM = "rhs_weap_fim92";
_SAMmag = "rhs_fim92_mag";

// Strzelec HAT
//_HAT = "rhs_weap_fgm148";
//_HATmag1 = "rhs_fgm148_magazine_AT";
//_HATmag2 = "rhs_fgm148_magazine_AT";

// Snajper
_SNrifle = "rhs_weap_M107";
_SNrifleMag = "rhsusf_mag_10Rnd_STD_50BMG_M33";

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";						// Spadochron

_bagFTL = "B_LIB_GER_A_frame";					// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_GER_A_frame";                  // Plecak dla pilota (radio)

_bagTL = "B_LIB_GER_A_frame";		//Plecak dowódcy drużyny

_bagMs = "fow_b_tornister_medic";						// Plecak dla medyka (mały)
_bagMm = "fow_b_tornister_medic";		// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_oli";        // Plecak dla medyka (duży)

_bagARs = "B_LIB_GER_Backpack";		// Plecak dla RKM (mały)
_bagARm = "B_LIB_GER_Backpack";					// Plecak dla RKM (średni)
_bagARb = "B_LIB_GER_Backpack";                   // Plecak dla RKM (duży)

_bagENG = "fow_b_ammoboxes";		// Plecak dla Mechanika

_bagR = "B_LIB_GER_A_frame";					// Plecak dla strzelca, strzelca AT

_bagMAT = "B_LIB_GER_Panzer_Empty";					// Plecak dla MAT

_bagmedium = "B_LIB_GER_Backpack";	// carries 200, weighs 30
_baglarge =  "B_Carryall_mcamo"; 				// carries 320, weighs 40

_bagmediumdiver =  "rhsusf_assault_eagleaiii_ucp";	// Plecaki SF

_baguav = "B_LIB_GER_Radio";					// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";			// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "LIB_GrWr34_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_GrWr34_Bag";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["fow_u_ger_m43_01_frag_private","fow_u_ger_m43_01_private"];
_baseHelmet = ["fow_h_ger_m40_heer_01","fow_h_ger_m40_heer_02","fow_h_ger_m40_heer_01_net","fow_h_ger_feldmutze"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_mediumRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_heavyRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];

// Siły specjalne
//_diverUniform =  ["rhs_uniform_FROG01_m81"];
//_diverHelmet = ["rhsusf_mich_bare_headset","rhsusf_ach_bare_headset_ess","rhsusf_ach_helmet_M81"];
//_diverRig = ["rhsusf_spc_marksman","rhsusf_spc_light"];
//_diverGlasses = [];

// Pilot
_pilotUniform = ["U_LIB_GER_LW_pilot"];
_pilotHelmet = ["H_LIB_GER_LW_PilotHelmet"];
_pilotRig = ["V_LIB_GER_PrivateBelt"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["fow_u_ger_tankcrew_01"];
_crewHelmet = ["H_LIB_GER_TankPrivateCap","H_LIB_GER_TankPrivateCap2","fow_h_ger_headset"];
_crewRig = ["fow_v_heer_tankcrew_p38"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["U_LIB_GER_LW_pilot"];
_sfhelmet = ["H_LIB_GER_LW_PilotHelmet"];
_sfRig = ["V_LIB_GER_PrivateBelt"];
_sfGlasses = [];

// Medyk
_medUniform =  ["fow_u_ger_m43_01_corporal"];
_medHelmet = ["fow_h_ger_m38_feldmutze"];
_medRig = ["fow_v_heer_k98"];
_medGlasses = [];

// Dowódca
_dowUniform =  ["fow_u_ger_m43_01_lance_corporal"];
_dowHelmet = ["fow_h_ger_officer_cap"];
_dowRig = ["fow_v_heer_mp40_nco"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);	// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================
	// Dodanie przedmiotów medycznych do każdej jednstki (gracza)	
	call Medical;
	
  };
  
// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_NVG;

	//WYWOŁANIE BUILDERA
	call Builder;

};
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	//SS
	case 2:
	{
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)
_attach2 = "rhsusf_acc_anpeq15_bk_light";	// Laser i latarka (można przełączyć) (RHS)
_attach3 = "rhsusf_acc_M952V";				// Latarka (RHS)

_silencer1 = "rhsusf_acc_nt4_black";		// Tłumik 5.56 (długi) (RHS)
_silencer2 = "rhsusf_acc_rotex5_tan";		// Tłumik 5.56 (krótki) (RHS)

_scope1 = "PSZ_AccO_EOT552";				// Eotech (Czarny PSZ)
_scope2 = "rhsusf_acc_compm4";				// Aimpoint (RHS)
_scope3 = "rhsusf_acc_LEUPOLDMK4_2";		// Celownik snajperski 6.5-20x

_bipod1 = "rhsusf_acc_harris_bipod";		// 
_bipod2 = "bipod_02_F_blk";					// 

// Jakie dodatki mają być dodane
_loadout_night_day = f_param_night_day_wyp;
if (_loadout_night_day == 0) then {			// Każda jednostka otrzyma ten zestaw dodatków
	_attachments = [_attach1,_scope1];		//NOC
} else {
	_attachments = [_attach3,_scope1];		//DZIEŃ 			
};

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];

// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "fow_w_g43";
_riflemag = "fow_10nd_792x57";
_riflemag_tr = "fow_10nd_792x57";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "fow_w_k98";
_carbinemag = "fow_5Rnd_792x57";
_carbinemag_tr = "fow_5Rnd_792x57";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
//_smg = "rhsusf_weap_MP7A1_base_f";
//_smgmag = "rhsusf_mag_40Rnd_46x30_FMJ";
//_smgmag_tr = "rhsusf_mag_40Rnd_46x30_FMJ";

// Broń z granatnikiem (dla dowóców)
_glrifle = "fow_w_stg44";
_glriflemag = "fow_30Rnd_792x33";
_glriflemag_tr = "fow_30Rnd_792x33";
//_glmag = "1Rnd_HE_Grenade_shell";

// Pistolet (dla wszystkich klas)
_pistol = "fow_w_p08";
_pistolmag = "fow_8Rnd_9x19";

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "fow_w_mg42";
_ARmag = "fow_50Rnd_792x57";
_ARmag_tr = "fow_50Rnd_792x57";

// Strzelec MMG
//_MMG = "rhs_weap_m240B";
//_MMGmag = "rhsusf_100Rnd_762x51";
//_MMGmag_tr = "rhsusf_100Rnd_762x51_m62_tracer";

// Strzelec wyborowy (broń DLC)
//_DMrifle = "srifle_DMR_06_camo_F";
//_DMriflemag = "20Rnd_762x51_Mag";

// Strzelec AT
_RAT = "LIB_PzFaust_30m";
//_RATmag = "";

// Strzelec MAT
_MAT = "LIB_RPzB";
_MATmag1 = "LIB_1Rnd_RPzB";
_MATmag2 = "LIB_1Rnd_RPzB";
//_MAT_sight = "rhs_weap_optic_smaw";

// Strzelec AA
//_SAM = "rhs_weap_fim92";
//_SAMmag = "rhs_fim92_mag";

// Strzelec HAT
//_HAT = "rhs_weap_fgm148";
//_HATmag1 = "rhs_fgm148_magazine_AT";
//_HATmag2 = "rhs_fgm148_magazine_AT";

// Snajper
_SNrifle = "rhs_weap_M107";
_SNrifleMag = "rhsusf_mag_10Rnd_STD_50BMG_M33";

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";						// Spadochron

_bagFTL = "B_LIB_GER_A_frame";					// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_GER_A_frame";                  // Plecak dla pilota (radio)

_bagTL = "B_LIB_GER_A_frame";		//Plecak dowódcy drużyny

_bagMs = "fow_b_tornister_medic";						// Plecak dla medyka (mały)
_bagMm = "fow_b_tornister_medic";		// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_oli";        // Plecak dla medyka (duży)

_bagARs = "B_LIB_GER_Backpack";		// Plecak dla RKM (mały)
_bagARm = "B_LIB_GER_Backpack";					// Plecak dla RKM (średni)
_bagARb = "B_LIB_GER_Backpack";                   // Plecak dla RKM (duży)

_bagENG = "fow_b_ammoboxes";		// Plecak dla Mechanika

_bagR = "B_LIB_GER_A_frame";					// Plecak dla strzelca, strzelca AT

_bagMAT = "B_LIB_GER_Panzer_Empty";					// Plecak dla MAT

_bagmedium = "B_LIB_GER_Backpack";	// carries 200, weighs 30
_baglarge =  "B_Carryall_mcamo"; 				// carries 320, weighs 40

_bagmediumdiver =  "rhsusf_assault_eagleaiii_ucp";	// Plecaki SF

_baguav = "B_LIB_GER_Radio";					// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";			// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "LIB_GrWr34_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_GrWr34_Bag";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["fow_u_ger_m43_peadot_01_private","fow_u_ger_m43_peadot_02_private"];
_baseHelmet = ["fow_h_ger_m40_ss_02","fow_h_ger_m40_ss_01"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_mediumRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_heavyRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];

// Siły specjalne
_diverUniform =  ["rhs_uniform_FROG01_m81"];
_diverHelmet = ["rhsusf_mich_bare_headset","rhsusf_ach_bare_headset_ess","rhsusf_ach_helmet_M81"];
_diverRig = ["rhsusf_spc_marksman","rhsusf_spc_light"];
_diverGlasses = [];

// Pilot
_pilotUniform = ["U_LIB_GER_LW_pilot"];
_pilotHelmet = ["H_LIB_GER_LW_PilotHelmet"];
_pilotRig = ["V_LIB_GER_PrivateBelt"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["fow_u_ger_tankcrew_01"];
_crewHelmet = ["H_LIB_GER_TankPrivateCap","H_LIB_GER_TankPrivateCap2","fow_h_ger_headset"];
_crewRig = ["fow_v_heer_tankcrew_p38"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["U_LIB_GER_LW_pilot"];
_sfhelmet = ["H_LIB_GER_LW_PilotHelmet"];
_sfRig = ["V_LIB_GER_PrivateBelt"];
_sfGlasses = [];

// Medyk
_medUniform =  ["fow_u_ger_m43_peadot_01_private","fow_u_ger_m43_peadot_02_private"];
_medHelmet = ["fow_h_ger_feldmutze_ss"];
_medRig = ["fow_v_heer_k98"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["fow_u_ger_m43_peadot_02_private"];
_dowHelmet = ["fow_h_ger_officer_cap_ss"];
_dowRig = ["fow_v_heer_mp40_nco"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];


// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)

	call Medical;
	
  };
  
// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_NVG;

	//WYWOŁANIE BUILDERA
	call Builder;

};	
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	//Fallschirmjäger
	case 3:
	{
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)
_attach2 = "rhsusf_acc_anpeq15_bk_light";	// Laser i latarka (można przełączyć) (RHS)
_attach3 = "rhsusf_acc_M952V";				// Latarka (RHS)

_silencer1 = "rhsusf_acc_nt4_black";		// Tłumik 5.56 (długi) (RHS)
_silencer2 = "rhsusf_acc_rotex5_tan";		// Tłumik 5.56 (krótki) (RHS)

_scope1 = "PSZ_AccO_EOT552";				// Eotech (Czarny PSZ)
_scope2 = "rhsusf_acc_compm4";				// Aimpoint (RHS)
_scope3 = "rhsusf_acc_LEUPOLDMK4_2";		// Celownik snajperski 6.5-20x

_bipod1 = "rhsusf_acc_harris_bipod";		// 
_bipod2 = "bipod_02_F_blk";					// 

// Jakie dodatki mają być dodane
_attachments = [_attach3,_scope2]; 			// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];

// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "fow_w_g43";
_riflemag = "fow_10nd_792x57";
_riflemag_tr = "fow_10nd_792x57";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "fow_w_k98";
_carbinemag = "fow_5Rnd_792x57";
_carbinemag_tr = "fow_5Rnd_792x57";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
//_smg = "rhsusf_weap_MP7A1_base_f";
//_smgmag = "rhsusf_mag_40Rnd_46x30_FMJ";
//_smgmag_tr = "rhsusf_mag_40Rnd_46x30_FMJ";

// Broń z granatnikiem (dla dowóców)
_glrifle = "fow_w_fg42";
_glriflemag = "fow_20Rnd_792x57";
_glriflemag_tr = "fow_20Rnd_792x57";
//_glmag = "1Rnd_HE_Grenade_shell";

// Pistolet (dla wszystkich klas)
_pistol = "fow_w_p08";
_pistolmag = "fow_8Rnd_9x19";

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "fow_w_mg42";
_ARmag = "fow_50Rnd_792x57";
_ARmag_tr = "fow_50Rnd_792x57";

// Strzelec MMG
//_MMG = "rhs_weap_m240B";
//_MMGmag = "rhsusf_100Rnd_762x51";
//_MMGmag_tr = "rhsusf_100Rnd_762x51_m62_tracer";

// Strzelec wyborowy (broń DLC)
//_DMrifle = "srifle_DMR_06_camo_F";
//_DMriflemag = "20Rnd_762x51_Mag";

// Strzelec AT
_RAT = "LIB_PzFaust_30m";
//_RATmag = "";

// Strzelec MAT
_MAT = "LIB_RPzB";
_MATmag1 = "LIB_1Rnd_RPzB";
_MATmag2 = "LIB_1Rnd_RPzB";
//_MAT_sight = "rhs_weap_optic_smaw";

// Strzelec AA
//_SAM = "rhs_weap_fim92";
//_SAMmag = "rhs_fim92_mag";

// Strzelec HAT
//_HAT = "rhs_weap_fgm148";
//_HATmag1 = "rhs_fgm148_magazine_AT";
//_HATmag2 = "rhs_fgm148_magazine_AT";

// Snajper
_SNrifle = "rhs_weap_M107";
_SNrifleMag = "rhsusf_mag_10Rnd_STD_50BMG_M33";

// ====================================================================================

// Plecaki
_bagsmall = "B_Parachute";						// Spadochron

_bagFTL = "B_LIB_GER_Backpack";					// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_GER_Backpack";                  	// Plecak dla pilota (radio)

_bagTL = "B_LIB_GER_Backpack";						//Plecak dowódcy drużyny

_bagMs = "B_LIB_GER_Backpack";		// Plecak dla medyka (mały)
_bagMm = "B_FieldPack_oli";						// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_oli";               			// Plecak dla medyka (duży)

_bagARs = "B_LIB_GER_Backpack";		// Plecak dla RKM (mały)
_bagARm = "B_LIB_GER_Backpack";						// Plecak dla RKM (średni)
_bagARb = "B_LIB_GER_Backpack";                     // Plecak dla RKM (duży)

_bagENG = "B_LIB_GER_Backpack";		// Plecak dla Mechanika

_bagR = "B_LIB_GER_Backpack";					// Plecak dla strzelca, strzelca AT

_bagMAT = "B_LIB_GER_Backpack";						// Plecak dla MAT

_bagmedium = "B_Kitbag_cbr";					// carries 200, weighs 30
_baglarge =  "B_Carryall_cbr"; 					// carries 320, weighs 40

_bagmediumdiver =  "rhsusf_assault_eagleaiii_coy";	// Plecaki SF

_baguav = "B_LIB_GER_Radio";							// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";			// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "LIB_GrWr34_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_GrWr34_Bag";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform =  ["fow_u_ger_fall_01_private","fow_u_ger_fall_01_lance_corporal","fow_u_ger_fall_01_corporal"];
_baseHelmet = ["fow_h_ger_m40_fall_01"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["fow_v_fall_bandoleer"];
_mediumRig = ["fow_v_fall_bandoleer"];
_heavyRig = ["fow_v_fall_bandoleer"];

// Siły specjalne
_diverUniform =  ["rhs_uniform_FROG01_m81"];
_diverHelmet = ["rhsusf_mich_bare_headset","rhsusf_ach_bare_headset_ess","rhsusf_ach_helmet_M81"];
_diverRig = ["rhsusf_spc_marksman","rhsusf_spc_light"];
_diverGlasses = [];

// Pilot
_pilotUniform = ["U_LIB_GER_pilot_LuftHptmM1908","U_LIB_GER_pilot_LuftLtM1908","U_LIB_GER_pilot_LuftOltM1908"];
_pilotHelmet = ["H_LIB_GER_PilotHelmet_LW_A","H_LIB_GER_PilotHelmet_LW_B","H_LIB_GER_PilotHelmet_LW_C"];
_pilotRig = ["V_LIB_GER_Vest_Vide_Luft"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["U_LIB_GER_Tank_Crew_Officer_Heer0tv0tpgcHptmMp40"];
_crewHelmet = ["H_LIB_GER_TankPrivateCap2","H_LIB_GER_TankPrivateCap"];
_crewRig = ["V_LIB_GER_TankPrivateBelt_0"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"]; 
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["U_LIB_GER_pilot_LuftHptmM1908","U_LIB_GER_pilot_LuftLtM1908","U_LIB_GER_pilot_LuftOltM1908"];
_sfhelmet = ["H_LIB_GER_PilotHelmet_LW_A","H_LIB_GER_PilotHelmet_LW_B","H_LIB_GER_PilotHelmet_LW_C"];
_sfRig = ["V_LIB_GER_Vest_Vide_Luft"];
_sfGlasses = [];

// Medyk
_medUniform =  ["fow_u_ger_fall_01_private"];
_medHelmet = ["fow_h_ger_m40_fall_01"];
_medRig = ["fow_v_fall_bandoleer"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["fow_u_ger_fall_01_sergeant"];
_dowHelmet = ["fow_h_ger_m40_fall_01"];
_dowRig = ["fow_v_fall_bandoleer"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["U_LIB_GER_Tank_Crew_Officer_Heer0tv0tpgcHptmMp40"];
_vcHelmet = ["H_LIB_GER_OfficerCap_PZWF"];
_vcRig = ["V_LIB_GER_OfficerVest_0A"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)

	call Medical;
	
  };
  
// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;

	//WYWOŁANIE BUILDERA
	call Builder;

};
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	//Wehrmacht - Winter
	case 4:
	{
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)
_attach2 = "rhsusf_acc_anpeq15_bk_light";	// Laser i latarka (można przełączyć) (RHS)
_attach3 = "rhsusf_acc_M952V";				// Latarka (RHS)

_silencer1 = "rhsusf_acc_nt4_black";		// Tłumik 5.56 (długi) (RHS)
_silencer2 = "rhsusf_acc_rotex5_tan";		// Tłumik 5.56 (krótki) (RHS)

_scope1 = "PSZ_AccO_EOT552";				// Eotech (Czarny PSZ)
_scope2 = "rhsusf_acc_compm4";				// Aimpoint (RHS)
_scope3 = "rhsusf_acc_LEUPOLDMK4_2";		// Celownik snajperski 6.5-20x

_bipod1 = "rhsusf_acc_harris_bipod";		// 
_bipod2 = "bipod_02_F_blk";					// 

// Jakie dodatki mają być dodane
_attachments = [_attach3,_scope2]; 			// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= []; 

// ====================================================================================

/// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "fow_w_g43";
_riflemag = "fow_10nd_792x57";
_riflemag_tr = "fow_10nd_792x57";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "fow_w_k98";
_carbinemag = "fow_5Rnd_792x57";
_carbinemag_tr = "fow_5Rnd_792x57";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
//_smg = "rhsusf_weap_MP7A1_base_f";
//_smgmag = "rhsusf_mag_40Rnd_46x30_FMJ";
//_smgmag_tr = "rhsusf_mag_40Rnd_46x30_FMJ";

// Broń z granatnikiem (dla dowóców)
_glrifle = "fow_w_mp40";
_glriflemag = "fow_32Rnd_9x19_mp40";
_glriflemag_tr = "fow_32Rnd_9x19_mp40";
//_glmag = "1Rnd_HE_Grenade_shell";

// Pistolet (dla wszystkich klas)
_pistol = "fow_w_p08";
_pistolmag = "fow_8Rnd_9x19";

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "fow_w_mg42";
_ARmag = "fow_50Rnd_792x57";
_ARmag_tr = "fow_50Rnd_792x57";

// Strzelec MMG
//_MMG = "rhs_weap_m240B";
//_MMGmag = "rhsusf_100Rnd_762x51";
//_MMGmag_tr = "rhsusf_100Rnd_762x51_m62_tracer";

// Strzelec wyborowy (broń DLC)
//_DMrifle = "srifle_DMR_06_camo_F";
//_DMriflemag = "20Rnd_762x51_Mag";

// Strzelec AT
_RAT = "LIB_PzFaust_30m";
//_RATmag = "";

// Strzelec MAT
_MAT = "LIB_RPzB";
_MATmag1 = "LIB_1Rnd_RPzB";
_MATmag2 = "LIB_1Rnd_RPzB";
//_MAT_sight = "rhs_weap_optic_smaw";

// Strzelec AA
_SAM = "rhs_weap_fim92";
_SAMmag = "rhs_fim92_mag";

// Strzelec HAT
//_HAT = "rhs_weap_fgm148";
//_HATmag1 = "rhs_fgm148_magazine_AT";
//_HATmag2 = "rhs_fgm148_magazine_AT";

// Snajper
_SNrifle = "rhs_weap_M107";
_SNrifleMag = "rhsusf_mag_10Rnd_STD_50BMG_M33";

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";						// Spadochron

_bagFTL = "B_LIB_GER_A_frame";					// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_GER_A_frame";                  // Plecak dla pilota (radio)

_bagTL = "B_LIB_GER_A_frame";		//Plecak dowódcy drużyny

_bagMs = "fow_b_tornister_medic";						// Plecak dla medyka (mały)
_bagMm = "fow_b_tornister_medic";		// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_oli";        // Plecak dla medyka (duży)

_bagARs = "B_LIB_GER_Backpack";		// Plecak dla RKM (mały)
_bagARm = "B_LIB_GER_Backpack";					// Plecak dla RKM (średni)
_bagARb = "B_LIB_GER_Backpack";                   // Plecak dla RKM (duży)

_bagENG = "fow_b_ammoboxes";		// Plecak dla Mechanika

_bagR = "B_LIB_GER_A_frame";					// Plecak dla strzelca, strzelca AT

_bagMAT = "B_LIB_GER_Panzer_Empty";					// Plecak dla MAT

_bagmedium = "B_LIB_GER_Backpack";	// carries 200, weighs 30
_baglarge =  "B_Carryall_mcamo"; 				// carries 320, weighs 40

_bagmediumdiver =  "rhsusf_assault_eagleaiii_ucp";	// Plecaki SF

_baguav = "B_LIB_GER_Radio";					// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";			// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "LIB_GrWr34_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_GrWr34_Bag";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["U_LIB_GER_Scharfschutze_w","U_LIB_GER_Soldier3_w"];
_baseHelmet = ["H_LIB_GER_Helmet_w","H_LIB_GER_Helmet_net_w","H_LIB_GER_Helmet_ns_w"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["V_LIB_GER_VestG43","V_LIB_GER_VestKar98","V_LIB_GER_VestMG","V_LIB_GER_VestMP40"];
_mediumRig = ["V_LIB_GER_VestG43","V_LIB_GER_VestKar98","V_LIB_GER_VestMG","V_LIB_GER_VestMP40"];
_heavyRig = ["V_LIB_GER_VestG43","V_LIB_GER_VestKar98","V_LIB_GER_VestMG","V_LIB_GER_VestMP40"];

// Siły specjalne
_diverUniform =  ["rhs_uniform_FROG01_m81"];
_diverHelmet = ["rhsusf_mich_bare_headset","rhsusf_ach_bare_headset_ess","rhsusf_ach_helmet_M81"];
_diverRig = ["rhsusf_spc_marksman","rhsusf_spc_light"];
_diverGlasses = [];

// Pilot
_pilotUniform = ["U_LIB_GER_LW_pilot"];
_pilotHelmet = ["H_LIB_GER_LW_PilotHelmet"];
_pilotRig = ["V_LIB_GER_PrivateBelt"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["fow_u_ger_tankcrew_01"];
_crewHelmet = ["H_LIB_GER_TankPrivateCap","H_LIB_GER_TankPrivateCap2","fow_h_ger_headset"];
_crewRig = ["fow_v_heer_tankcrew_p38"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"]; //DLC alternatives: ["U_B_FullGhillie_lsh","U_B_FullGhillie_ard","U_B_FullGhillie_sard"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["U_LIB_GER_LW_pilot"];
_sfhelmet = ["H_LIB_GER_LW_PilotHelmet"];
_sfRig = ["V_LIB_GER_PrivateBelt"];
_sfGlasses = [];

// Medyk
_medUniform =  ["U_LIB_GER_Scharfschutze_w"];
_medHelmet = ["H_LIB_GER_Fieldcap"];
_medRig = ["V_LIB_GER_VestKar98"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["U_LIB_GER_Soldier_camo_w"];
_dowHelmet = ["H_LIB_GER_Ushanka"];
_dowRig = ["V_LIB_GER_FieldOfficer"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];


// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)

	call Medical;
		
  };  
  
// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;

	//WYWOŁANIE BUILDERA
	call Builder;

};
	
//*****************************************************************************************************
//+++++++++++++WIP+++++++++++WIP++++++++WIP+++++++++WIP++++++++++++WIP++++++++++++WIP++++++++++++++++++
//*****************************************************************************************************

	//Japan Imperial Army
	case 5:
	{
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)
_attach2 = "rhsusf_acc_M952V";				// Latarka (RHS)
_attach3 = "acc_pointer_IR";				// czerwony laser (ARMA)

//_silencer1 = "muzzle_snds_M";				// Tłumik 5.56 (długi)
//_silencer2 = "BWA3_muzzle_snds_G36";		// Tłumik 5.56 (krótki)

_scope1 = "PSZ_AccO_EOT552";				// Eotech (PSZ)
_scope2 = "rhsusf_acc_compm4";				// Aimpoint (RHS)
_scope3 = "rhsusf_acc_LEUPOLDMK4_2";		// Celownik snajperski 6.5-20x (RHS)

_bipod1 = "";								// 
_bipod2 = "bipod_02_F_blk";					// 

// Jakie dodatki mają być dodane
_attachments = [_attach2,_scope1]; 			// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================

// Dodatki do pistoletu
_hg_silencer1 = "muzzle_snds_acp";	// .45 suppressor
_hg_silencer2 = "muzzle_snds_L";	// SF pistol suppressor
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];

// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "fow_w_type99";
_riflemag = "fow_5Rnd_77x58";
_riflemag_tr = "fow_5Rnd_77x58";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "fow_w_type99";
_carbinemag = "fow_5Rnd_77x58";
_carbinemag_tr = "fow_5Rnd_77x58";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
//_smg = "rhs_weap_aks74u";
//_smgmag = "rhs_30Rnd_545x39_AK";
//_smgmag_tr = "rhs_30Rnd_545x39_AK_green";

// Siły specjalne
//_diverWepCaS = "PSZ_Radon_HG";
//_diverMagCaS = "PSZ_556x45_STANAG_30";
//_diverWepM = "PSZ_Radon_HG";
//_diverMagM = "PSZ_556x45_STANAG_30";
//_diverWepR = "PSZ_Radon";
//_diverMagR = "PSZ_556x45_STANAG_30";
//_secendWep = "PSZ_P99";
//_secendMag = "PSZ_9x19_P99_16";

// Broń z granatnikiem (dla dowóców)
_glrifle = "fow_w_type100";
_glriflemag = "fow_32Rnd_8x22";
_glriflemag_tr = "fow_32Rnd_8x22";
//_glmag = "PSZ_40x47_NGO74_HE";

// Pistolet (dla wszystkich klas)
_pistol = "fow_w_type14";
_pistolmag = "fow_8Rnd_8x22";

//Granatnik ręczny
//_pistol_GL= "rhs_weap_M320";

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "fow_w_type99_lmg";
_ARmag = "fow_30Rnd_77x58";
_ARmag_tr = "fow_30Rnd_77x58";

// Strzelec MMG
//_MMG = "rhs_weap_pkm";
//_MMGmag = "rhs_100Rnd_762x54mmR";
//_MMGmag_tr = "rhs_100Rnd_762x54mmR_green";

// Strzelec wyborowy (broń DLC)
//_DMrifle = "srifle_DMR_06_camo_F";
//_DMriflemag = "20Rnd_762x51_Mag";

// Strzelec AT
//_RAT = "rhs_weap_rpg26";
//_RATmag = "rhs_rpg26_mag";

// Strzelec MAT
//_MAT = "rhs_weap_rpg7";
//_MATmag1 = "rhs_rpg7_PG7VL_mag";
//_MATmag2 = "rhs_rpg7_PG7VR_mag";
//_MAT_sight = "rhs_acc_pgo7v";

// Strzelec AA
//_SAM = "PSZ_Grom";
//_SAMmag = "PSZ_Grom_AA";

// Strzelec HAT
//_HAT = "PSZ_Spike_LR";
//_HATmag1 = "PSZ_Spike_THEAT";
//_HATmag2 = "PSZ_Spike_THEAT";

// Snajper
//_SNrifle = "srifle_LRR_F";
//_SNrifleMag = "7Rnd_408_Mag";

// ====================================================================================

// Plecaki

_bagsmall = "DEGA_T10_Parachute_backpack";		// Spadochron

_bagFTL = "fow_b_ija_backpack";						// Plecak dla dowódcy (radio)
_bagPP = "fow_b_ija_backpack";                  	// Plecak dla pilota (radio)

_bagTL = "fow_b_ija_backpack";						//Plecak dowódcy drużyny

_bagMs = "B_FieldPack_cbr";					// Plecak dla medyka (mały)
_bagMm = "B_FieldPack_cbr";					// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_cbr";              // Plecak dla medyka (duży)

_bagARs = "fow_b_ija_ammobox_metal";					// Plecak dla RKM (mały)
_bagARm = "fow_b_ija_ammobox_metal";						// Plecak dla RKM (średni)
_bagARb = "fow_b_ija_ammobox_metal";						// Plecak dla RKM (duży)

_bagENG = "fow_b_ija_ammobox_metal";					// Plecak dla Mechanika

_bagR = "fow_b_ija_backpack";						// Plecak dla strzelca, strzelca AT

_bagMAT = "fow_b_ija_ammobox_metal";						// Plecak dla MAT

_bagmedium = "B_TacticalPack_oli";				// carries 200, weighs 30
_baglarge =  "PSZ_B_wz97_DES"; 					// carries 320, weighs 40

_bagmediumdiver =  "B_FieldPack_cbr";			// Plecaki SF

_baguav = "B_LIB_GER_Radio";						// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";						// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";				// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "B_Mortar_01_weapon_F";				// used by Mortar gunner
_bagmtrag = "B_Mortar_01_support_F";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["fow_u_ija_type98","fow_u_ija_type98_short"];
_baseHelmet = ["fow_h_ija_type90","fow_h_ija_type90_net_neck"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["fow_v_ija_mg","fow_v_ija_rifle","fow_v_ija_grenadier"];
_mediumRig = ["fow_v_ija_mg","fow_v_ija_rifle","fow_v_ija_grenadier"]; 	
_heavyRig = ["fow_v_ija_mg","fow_v_ija_rifle","fow_v_ija_grenadier"];

// Siły specjalne
//_diverUniform =  ["PSZ_U_PL_DES_wz2010_Crye_Folded","PSZ_U_PL_DES_wz2010_Polar"];
//_diverHelmet = ["PSZ_H_wz2005_OLIVE_ESS","PSZ_H_wz2005_DES_ESS", "PSZ_H_wz2005_DES"];
//_diverRig = ["PSZ_V_UKO_L_DES_M_Headset","PSZ_V_UKO_L_DES_R_Headset", "PSZ_V_UKO_L_WDL_R_Headset"];
//_diverGlasses = [];

// Pilot
_pilotUniform = ["fow_u_ija_pilot"];
_pilotHelmet = ["fow_h_ija_flight_helmet"];
_pilotRig = ["fow_v_ija_bayonet"];
_pilotGlasses = [];

// JET Pilot
_sfuniform = ["fow_u_ija_pilot"];
_sfhelmet = ["fow_h_ija_flight_helmet"];
_sfRig = ["fow_v_ija_bayonet"];
_sfGlasses = [];

// Załoga pojazdu
_crewUniform = ["fow_u_ija_type98_short"];
_crewHelmet = ["fow_h_ija_tank_helmet"];
_crewRig = ["fow_v_ija_bayonet"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"]; //DLC alternatives: ["U_B_FullGhillie_lsh","U_B_FullGhillie_ard","U_B_FullGhillie_sard"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// Medyk
_medUniform =  ["fow_u_ija_type98"];
_medHelmet = ["fow_h_ija_type90"];
_medRig = ["fow_v_ija_medic"];
_medGlasses = [""];

// Dowódca
_dowUniform =  ["fow_u_ija_type98_khakibrown"];
_dowHelmet = ["fow_h_ija_fieldcap_neck","fow_h_ija_fieldcap_officer"];
_dowRig = ["fow_v_ija_nco","fow_v_ija_officer"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ija_type98_short"];
_vcHelmet = ["fow_h_ija_tank_helmet"];
_vcRig = ["fow_v_ija_bayonet"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);			// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================

	// Przedmioty uniwersalne dla klas
	// Dodanie przedmiotów do każdej jednstki (gracza)

	call Medical;
		
  };
  
// ====================================================================================
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;

	//WYWOŁANIE BUILDERA
	call Builder;

};	
		
// ====================================================================================
// ====================================================================================
//EMD CASE STATEMENT		

};

// ====================================================================================
// ====================================================================================
// If this isn't run on an infantry unit we can exit
if !(_isMan) exitWith {};

// ====================================================================================
// Handle weapon attachments
#include "f_assignGear_attachments.sqf";

// ====================================================================================
// ENSURE UNIT HAS CORRECT WEAPON SELECTED ON SPAWNING
_unit selectweapon primaryweapon _unit;