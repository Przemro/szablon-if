// ====================================================================================
// S.D.S Assign Gear Script - [V-1.8 beta | 04.01.2018]
// ====================================================================================

//	SPIS TRESCI
/*

	1.	Wehrmacht

	
*/
// ====================================================================================
	_loadout_faction_player = f_param_player_faction_Blufor;
// ====================================================================================

//Definicje przedmiotów

// Sprzet medyczny
_personalAidKit = "ACE_personalAidKit";		// Zestaw pierwszej pomocy
_surgicalKit = "ACE_surgicalKit";			// Zestaw do szycia ran
_AED = "adv_aceCPR_AED";					// Defibrylator
_splint = "adv_aceSplint_splint";			// Szyny do usztwniania

_bandage = "ACE_fieldDressing";				// Ogólny bandaż dla piechoty
_bandage_elastic = "ACE_elasticBandage";	// Bandaż elastyczny
_bandage_packing = "ACE_packingBandage";	//
_bandage_quikclot = "ACE_quikclot";			// Opatrunek "QuikClot"
_tourniquet ="ACE_tourniquet";				// Opaska uciskowa

_epinephrine = "ACE_epinephrine";			// Epinefryna (zwiększa tętno)
_morphine = "ACE_morphine";					// Morfina (niweluje ból, rozrzedza krew)
_adenosine = "ACE_adenosine";				// Adenozyna (obniża tętno)
_atropine = "ACE_atropine";					// Atropina (obniża tętno)

_blood_s = "ACE_bloodIV_250";				// Krew 200 ml
_blood_m = "ACE_bloodIV_500";				// Krew 500 ml
_blood_b = "ACE_bloodIV";					// Krew 1000 ml

_plasma_s = "ACE_plasmaIV_250";				// Osocze 200 ml
_plasma_m = "ACE_plasmaIV_500";				// Osocze 500 ml
_plasma_b = "ACE_plasmaIV";					// Osocze 1000 ml

_saline_s = "ACE_salineIV_250";				// Sól fizjologiczna 200 ml
_saline_m = "ACE_salineIV_500";				// Sól fizjologiczna 500 ml
_saline_b = "ACE_salineIV";					// Sól fizjologiczna 1000 ml

_firstaid = "FirstAidKit";
_medkit = "Medikit";

//Radia ACRE2
_radioSR = "ACRE_PRC343";
_radioMR = "ACRE_PRC152";
_radioLR = "ACRE_PRC77";

//Przydatne rzeczy
_earplugs = "ACE_EarPlugs";
_IRstrobe = "ACE_IR_Strobe_Item";
_latarka = "ACE_Flashlight_XL50";
_lornetkaAM = "ACE_Vector";
_lornetkaFTL = "Binocular";
_lornetkaRTO = "Binocular";
_GPS = "ItemGPS";
_handcuffs = "ACE_CableTie";
_worek = "ACE_bodyBag";
_huntIR = "ACE_HuntIR_M203";
_huntIR_tab = "ACE_HuntIR_monitor";
_wissle = "fow_i_whistle";

// Flary ręczne
_flarewhite = "ACE_HandFlare_White";
_flarered = "ACE_HandFlare_Red";
_flareyellow = "ACE_HandFlare_Yellow";
_flaregreen = "ACE_HandFlare_Green";

/*
// Świetliki
_chemgreen =  "Chemlight_green";
_chemred = "Chemlight_red";
_chemyellow =  "Chemlight_yellow";
_chemblue = "Chemlight_blue";
*/

//Granaty dymne
_smokegrenade = "LIB_NB39";
_smokegrenadegreen = "SmokeShellGreen";
_smokegrenadeblue = "SmokeShellBlue";
_smokegrenadered = "SmokeShellRed";

// Granaty ręczne
_grenade = "LIB_Shg24";
_mgrenade = "ACE_M84";

/*
// Granaty dymne do granatnika
_glsmokewhite = "1Rnd_Smoke_Grenade_shell";
_glsmokegreen = "1Rnd_SmokeGreen_Grenade_shell";
_glsmokered = "1Rnd_SmokeRed_Grenade_shell";

// Flary do granatnika
_glflarewhite = "CUP_1Rnd_StarFlare_White_M203";
_glflarered = "CUP_1Rnd_StarFlare_Red_M203";
_glflareyellow = "UGL_FlareYellow_F";
_glflaregreen = "CUP_1Rnd_StarFlare_Green_M203";
*/

//Przedmioty w skrzyniach
_satche_small = "LIB_Ladung_Small_MINE_mag";
_satche_big = "LIB_Ladung_Big_MINE_mag";
_satche_extra_big = "SatchelCharge_Remote_Mag";
_toolkit = "ToolKit";
_zapalnik = "ACE_LIB_FireCord";
_zapalnik_b = "ACE_LIB_LadungPM";
_n_rozbraja = "ACE_DefusalKit";
_saperka = "ACE_EntrenchingTool";
_mlotek = "ACE_Fortify";
_wirecutter = "ACE_wirecutter";

// Mechanicy/saperzy
_ATmine = "ATMine_Range_Mag";
_APmine1 = "APERSBoundingMine_Range_Mag";
_APmine2 = "APERSMine_Range_Mag";
_mineDetector = "ACE_VMM3";

// Noktowizja
_nvg = "CUP_NVG_PVS15_black";

// Terminal UAV
_uavterminal = "B_UavTerminal";	  // Dla BLUFORU musi być terminal BLUFORU, innej frakcji nie zadziała

//Konstruktor BUILDERA
Builder = {	
	_backpack = {
		_typeofBackPack = _this select 0;
		_loadout = f_param_backpacks;
		if (count _this > 1) then {_loadout = _this select 1;};
		switch (_typeofBackPack) do
		{
			#include "f_assignGear_nato_b.sqf";
		};
	};
// SETUP CRATE
	_crate = {
		_typeofCrate = _this select 0;
		switch (_typeofCrate) do
		{
			#include "f_assignGear_crate.sqf";
		};
	};
// WYWOŁANIE BUILDERA
		#include "f_assignGear_nato_builder.sqf";
};	

//Dodanie wyposażenia medycznego	
Medical = {
	for "_p" from 1 to 4 do {_unit addItem _bandage;};		// Dodanie 8 sztuk bandaży	
	_unit linkItem "ItemMap";		// Dodanie mapy
	_unit linkItem "ItemCompass";	// Dodanie kompsu
	_unit linkItem "ItemWatch";		// Dodanie zegarka
	_unit addItem _earplugs;		// Dodaje zatyczki do uszy
	_unit addItem _latarka;			// Latarka na mape
};

//Nocne wyposażenie oparte na noktowizji, laserach, flarach
Night_Eq_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
			_unit addItem _IRstrobe;		// Znacznik IR (doczepiany)
			_unit addItem _IRstrobe;		
			(unitBackpack _unit) addItemCargoGlobal [_chemblue,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarewhite,4];
		(unitBackpack _unit) addMagazineCargoGlobal [_glflarered,4];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji	
		};
	};
};

//Nocne wyposażenie oparte na latarkach, flarach, lightstick-ach
Night_Eq_No_NVG = {
	_loadout_night_day = f_param_night_day_wyp;
		
	if (_loadout_night_day == 0) then {
		if(_typeofUnit != "ps") then {
		
			(unitBackpack _unit) addItemCargoGlobal [_flarered,2];
			(unitBackpack _unit) addItemCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "dc" || _typeofUnit == "co" || _typeofUnit == "ftl" || _typeofUnit == "uav") then {
		(unitBackpack _unit) addMagazineCargoGlobal [_flarered,2];
		(unitBackpack _unit) addMagazineCargoGlobal [_flarewhite,3];
		};
		
		if (_typeofUnit == "pp" || _typeofUnit == "ps" || _typeofUnit == "pcc" || _typeofUnit == "pc" || _typeofUnit == "divc" || _typeofUnit == "divm" || _typeofUnit == "divs" || _typeofUnit == "divr") then {
			_unit linkItem _nvg;			// Dodanie noktowizji
		};
	};
};
	

// ====================================================================================

switch (_loadout_faction_player) do
{
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************

	//Wehrmacht
	case 1: 
	{
	// Dodatki broń główna
_attach1 = "rhsusf_acc_anpeq15side_bk";		// Laser (RHS)

_silencer1 = "LIB_ACC_K98_Bayo";		// Tłumik 5.56 (długi) (RHS)

_scope1 = "rhsusf_acc_eotech_552";			// Eotech (Czarny PSZ)

_bipod1 = "rhsusf_acc_harris_bipod";		// 

// Jakie dodatki mają być dodane
_attachments = [_silencer1]; 	// Każda jednostka otrzyma ten zestaw dodatków

// ====================================================================================
// Dodatki do pistoletu
_hg_silencer1 = "rhsusf_acc_omega9k";	// 9mm tłumik do Glocka 17
_hg_attah1 = "acc_flashlight_pistol";
_hg_scope1 = "optic_MRD";			// MRD
_hg_attachments= [];
// ====================================================================================

// Bronie

// Podstawowa broń (dla przykładowo: Amunicyjny MMG, Amunicyjny RKM, Amunicyjny MAT,Strzelec)
_rifle = "LIB_K98_Late";
_riflemag = "LIB_5Rnd_792x57";
_riflemag_tr = "LIB_5Rnd_792x57_t";

// Broń wersja "krótka" (dla przykładowo: Medyk, Strzelec (AT), Strzelec MAT)
_carbine = "LIB_G43";
_carbinemag = "LIB_10Rnd_792x57";
_carbinemag_tr = "LIB_10Rnd_792x57_T";

// Broń PDW (dla przykładowo: Pilot, Co-pilot,Dowódca pojazdu)
_smg = "LIB_MP38";
_smgmag = "LIB_32Rnd_9x19";
_smgmag_tr = "LIB_32Rnd_9x19";

// Broń z granatnikiem (dla dowóców)
_glrifle = "LIB_MP38";
_glriflemag = "LIB_32Rnd_9x19";
_glriflemag_tr = "LIB_32Rnd_9x19";
//_glmag = "rhs_VOG25";

// Pistolet (dla wszystkich klas)
_pistol = "LIB_P38";
_pistolmag = "LIB_8Rnd_9x19";

/*
// Siły specjalne
_diverWepCaS = "arifle_min_rf_ak12_camo_grip";
_diverMagCaS = "30Rnd_min_rf_545x39_mag";
_diverWepR = "arifle_min_rf_ak12_camo_grip";
_diverMagR = "30Rnd_min_rf_545x39_mag";
_diverWepM = "arifle_min_rf_ak12_camo";
_diverMagM = "30Rnd_min_rf_545x39_mag";
*/

// ====================================================================================

// Sprzęt dla klas funkcyjnych

// Strzelec RKM
_AR = "LIB_MG34";
_ARmag = "LIB_50Rnd_792x57";
_ARmag_tr = "LIB_50Rnd_792x57_SMK";

/*
// Strzelec MMG
_MMG = "rhs_weap_m240B";
_MMGmag = "rhsusf_100Rnd_762x51";
_MMGmag_tr = "rhsusf_100Rnd_762x51_m62_tracer";
*/

// Strzelec wyborowy
_DMrifle = "LIB_K98ZF39";
_DMriflemag = "LIB_5Rnd_792x57";

// Strzelec AT
_RAT = "LIB_PzFaust_60m";
//_RATmag = "";

// Strzelec MAT
_MAT = "LIB_RPzB";
_MATmag1 = "LIB_1Rnd_RPzB";
_MATmag2 = "LIB_1Rnd_RPzB";
//_MAT_sight = "rhs_optic_maaws";
//_MATsptr = "rhs_mag_smaw_SR";

/*
// Strzelec AA
_SAM = "rhs_weap_fim92";
_SAMmag = "rhs_fim92_mag";

// Strzelec HAT
_HAT = "rhs_weap_fgm148";		
_HATmag1 = "rhs_fgm148_magazine_AT";
_HATmag2 = "rhs_fgm148_magazine_AT";
_HAT_Mobile = true;			//true - Javelin		false - TOW

// Snajper
_SNrifle = "rhs_weap_M107";
_SNrifleMag = "rhsusf_mag_10Rnd_STD_50BMG_M33";
*/

// ====================================================================================

// Plecaki

_bagsmall = "B_Parachute";						// Spadochron

_bagFTL = "B_LIB_GER_A_frame";					// Plecak dla dowódcy (radio)
_bagPP = "B_LIB_GER_A_frame";                  // Plecak dla pilota (radio)

_bagTL = "B_LIB_GER_A_frame";		//Plecak dowódcy drużyny

_bagMs = "fow_b_tornister_medic";						// Plecak dla medyka (mały)
_bagMm = "fow_b_tornister_medic";		// Plecak dla medyka (średni)
_bagMb = "B_FieldPack_oli";        // Plecak dla medyka (duży)

_bagARs = "B_LIB_GER_Backpack";		// Plecak dla RKM (mały)
_bagARm = "B_LIB_GER_Backpack";					// Plecak dla RKM (średni)
_bagARb = "B_LIB_GER_Backpack";                   // Plecak dla RKM (duży)

_bagENG = "fow_b_ammoboxes";		// Plecak dla Mechanika

_bagR = "B_LIB_GER_A_frame";					// Plecak dla strzelca, strzelca AT

_bagMAT = "B_LIB_GER_Panzer_Empty";					// Plecak dla MAT

_bagmedium = "B_LIB_GER_Backpack";	// carries 200, weighs 30
_baglarge =  "B_Carryall_mcamo"; 				// carries 320, weighs 40

_bagmediumdiver =  "rhsusf_assault_eagleaiii_ucp";	// Plecaki SF

_baguav = "B_LIB_GER_Radio";					// Plecak RTO

_baghmgg = "RHS_M2_Gun_Bag";					// used by Heavy MG gunner
_baghmgag = "RHS_M2_MiniTripod_Bag";			// used by Heavy MG assistant gunner

_baghatg = "B_AT_01_weapon_F";					// used by Heavy AT gunner
_baghatag = "B_HMG_01_support_F";				// used by Heavy AT assistant gunner
_bagmtrg = "LIB_GrWr34_Bar";				// used by Mortar gunner
_bagmtrag = "LIB_GrWr34_Bag";			// used by Mortar assistant gunner
_baghsamg = "B_AA_01_weapon_F";					// used by Heavy SAM gunner
_baghsamag = "B_HMG_01_support_F";				// used by Heavy SAM assistant gunner

// ====================================================================================

// Mundury, kamizelki itp.

// Definicja jaka klasa do jakiej grupy ma się zaliczać
// Jeżeli jakaś klasa nie jest tutaj dopisana domyslnie trafi do "_medium"

_light = [];
_heavy =  ["eng","engm"];
_diver = ["divc","divr","divs","divm"];
_pilot = ["pp","pcc","pc"];
_crew = ["vc","vg","vd"];
_ghillie = ["sn","sp"];
_specOp = ["ps"];
_med = ["m"];
_dow = ["co","dc"];
_vc = ["vc"];

// Podstawowe mundury
// Elementy wyposażenia są losowo wybierane z listy
_baseUniform = ["fow_u_ger_m43_01_frag_private","fow_u_ger_m43_01_private"];
_baseHelmet = ["fow_h_ger_m40_heer_01","fow_h_ger_m40_heer_02","fow_h_ger_m40_heer_01_net","fow_h_ger_feldmutze"];
_baseGlasses = [];

// Kamizelki
_lightRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_mediumRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];
_heavyRig = ["fow_v_heer_k98","fow_v_heer_g43","fow_v_heer_mg","fow_v_heer_k98_light"];

/*
// Siły specjalne
_diverUniform =  ["rhs_uniform_gorka_r_y"];
_diverHelmet = ["rhs_ssh68"];
_diverRig = ["rhs_vydra_3m"];
_diverGlasses = [];
*/

// Pilot
_pilotUniform = ["U_LIB_GER_LW_pilot"];
_pilotHelmet = ["H_LIB_GER_LW_PilotHelmet"];
_pilotRig = ["V_LIB_GER_PrivateBelt"];
_pilotGlasses = [];

// Załoga pojazdu
_crewUniform = ["fow_u_ger_tankcrew_01"];
_crewHelmet = ["H_LIB_GER_TankPrivateCap","H_LIB_GER_TankPrivateCap2","fow_h_ger_headset"];
_crewRig = ["fow_v_heer_tankcrew_p38"];
_crewGlasses = [];

// Ghillie
_ghillieUniform = ["U_B_GhillieSuit"];
_ghillieHelmet = [];
_ghillieRig = ["V_Chestrig_rgr"];
_ghillieGlasses = [];

// JET Pilot
_sfuniform = ["U_LIB_GER_LW_pilot"];
_sfhelmet = ["H_LIB_GER_LW_PilotHelmet"];
_sfRig = ["V_LIB_GER_PrivateBelt"];
_sfGlasses = [];

// Medyk
_medUniform =  ["fow_u_ger_m43_01_corporal"];
_medHelmet = ["fow_h_ger_m38_feldmutze"];
_medRig = ["fow_v_heer_k98"];
_medGlasses = [];

// Dowódca
_dowUniform =  ["fow_u_ger_m43_01_lance_corporal"];
_dowHelmet = ["fow_h_ger_officer_cap"];
_dowRig = ["fow_v_heer_mp40_nco"];
_dowGlasses = [];

// Dowódca pojazdu
_vcUniform =  ["fow_u_ger_tankcrew_01_2nd_leutnant"];
_vcHelmet = ["H_LIB_GER_TankOfficerCap2","H_LIB_GER_TankOfficerCap"];
_vcRig = ["fow_v_heer_tankcrew_p38"];
_vcGlasses = [];

// ====================================================================================

// INTERPRET PASSED VARIABLES
// The following inrerprets formats what has been passed to this script element

_typeofUnit = toLower (_this select 0);	// Tidy input for SWITCH/CASE statements, expecting something like : r = Rifleman, co = Commanding Officer, rat = Rifleman (AT)
_unit = _this select 1;					// expecting name of unit; originally passed by using 'this' in unit init
_isMan = _unit isKindOf "CAManBase";	// We check if we're dealing with a soldier or a vehicle

// ====================================================================================

// This block needs only to be run on an infantry unit
if (_isMan) then {

	// PREPARE UNIT FOR GEAR ADDITION
	// The following code removes all existing weapons, items, magazines and backpacks

	removeBackpack _unit;
	removeAllWeapons _unit;
	removeAllItemsWithMagazines _unit;
	removeAllAssignedItems _unit;

// ====================================================================================

	// HANDLE CLOTHES
	// Handle clothes and helmets and such using the include file called next.

	#include "f_assignGear_clothes.sqf";

// ====================================================================================
	// Dodanie przedmiotów medycznych do każdej jednstki (gracza)	
	call Medical;
	
  };
  
// ====================================================================================
	//WYWOŁANIE BUILDERA
	call Builder;
	
	//Dodanie nocnego wyposażenia
	call Night_Eq_No_NVG;

};
	
//*****************************************************************************************************
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//*****************************************************************************************************



// ====================================================================================
// ====================================================================================
//END CASE STATEMENT		

};

// ====================================================================================
// ====================================================================================
// If this isn't run on an infantry unit we can exit
if !(_isMan) exitWith {};

// ====================================================================================
// Handle weapon attachments
#include "f_assignGear_attachments.sqf";

// ====================================================================================
// ENSURE UNIT HAS CORRECT WEAPON SELECTED ON SPAWNING
_unit selectweapon primaryweapon _unit;